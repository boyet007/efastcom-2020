const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/admin-custom.scss', 'public/admin/css/custom.css')
    .sass('resources/sass/web-custom.scss', 'public/web/css/custom.css')
    .js('resources/js/web-custom.js', 'public/web/js/custom.js');