<?php

use Illuminate\Support\Facades\Route;
use App\Mail\VerificationEmail;
use Illuminate\Support\Facades\Mail;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('index');
Route::match(['get', 'post'], '/dapatkan-kategori-produk', 'SiteController@dapatkanKategoriProduk')->name('dapatkan.kategori.produk');
Route::get('/produk', 'SiteController@dapatkanProduk')->name('dapatkan.produk');
Route::get('/produk/{id}', 'SiteController@spesifikProduk')->name('spesifik.produk');
Route::get('/produk-promo', 'SiteController@dapatkanProdukPromo')->name('dapatkan.produk.promo');
Route::get('/login-user', 'UserController@loginUser')->name('login.user');
Route::get('/daftar-baru', 'UserController@daftarUserBaru')->name('daftar.user.baru');
Route::get('/verifikasi-token/{token}', 'Auth\RegisterController@verifikasiToken')->name('verifikasi.token');
Route::get('/verifikasi-ulang/{id}', 'Auth\LoginController@verifikasiUlang')->name('verifikasi.ulang');
Route::post('/login-user', 'Auth\LoginController@authenticate')->name('post.login.user');
Route::get('/web-kategori-produk/{id}', 'SiteController@webKategoriProduk')->name('web.kategori.produk');
Route::post('/daftar-baru', 'Auth\RegisterController@create')->name('post.daftar.baru');

Route::group(['middleware' => ['auth', 'checkRole:admin,user', 'checkVerify:true']], function() {
    Route::post('/tambah-keranjang', 'BelanjaController@tambahKeranjang')->name('tambah.keranjang');
    Route::get('/hapusItemKeranjang/{id}', 'BelanjaController@hapusItemKeranjang')->name('hapus.item.keranjang');
    Route::get('/cekout', 'BelanjaController@cekout')->name('cekout');
    Route::post('/dapatkan-list-paket', 'BelanjaController@dapatkanListPaket')->name('dapatkan.list.paket');
    Route::post('/hitung-biaya-kirim', 'BelanjaController@hitungBiayaKirim')->name('hitung.biaya.kirim');
    Route::post('/proses-pesanan', 'BelanjaController@prosesPesanan')->name('proses.pesanan');    
});

Route::group(['middleware' => ['auth', 'checkRole:admin,user']], function() {
    Route::get('/profile/{id}', 'UserController@profile')->name('profile');
    Route::get('/rubah-password/{id}', 'UserController@rubahPassword')->name('rubah.password');
    Route::post('/update-user/{user}', 'UserController@updateUser')->name('update.user');
    Route::post('/rubah-password/{user}', 'UserController@rubahPassword')->name('rubah.password');
    
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});

//miscelaneous
Route::post('/cari-produk-berdasarkan-kategori-id', 'ProdukController@cariProdukBerdasarkanKategoriId')->name('cari.produk.berdasarkan.kategori.id');
Route::post('/cari-provinsi-id', 'UserController@cariProvinsiId')->name('cari.provinsi.id');
Route::post('/cari-list-kota', 'UserController@cariListKota')->name('cari.list.kota');

Route::group(['middleware' => ['auth', 'checkRole:admin', 'checkVerify:true']], function() {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

        //master
        Route::resource('satuan', 'SatuanController');
        Route::resource('kota', 'KotaController');
        Route::resource('kategori-user', 'KategoriUserController');
        Route::resource('merek', 'MerekController');
        Route::resource('user', 'UserController');
        Route::resource('jasa-kirim', 'JasaKirimController');
        Route::resource('kategori-produk', 'KategoriProdukController');
        Route::resource('admin-produk', 'ProdukController');

        //transaksi
        Route::get('order', 'OrderController@index')->name('order.index');
        
        //table
        Route::get('tabel-satuan', 'SatuanController@dataTable')->name('table.satuan');
        Route::get('tabel-kota', 'KotaController@dataTable')->name('table.kota');
        Route::get('tabel-merek', 'MerekController@dataTable')->name('table.merek');
        Route::get('tabel-kategori-produk', 'KategoriProdukController@dataTable')->name('table.kategori.produk');
        Route::get('tabel-produk', 'ProdukController@dataTable')->name('table.produk');
        Route::get('tabel-user', 'UserController@dataTable')->name('table.user');
        Route::get('tabel-order', 'OrderController@dataTable')->name('table.order');

        //miscelaneous
        Route::post('/cari-data-produk', 'ProdukController@cariDataProduk')->name('cari.data.produk');
        Route::post('/cari-nama-satuan', 'ProdukController@cariNamaSatuan')->name('cari.nama.satuan');
        Route::post('/cari-merek-logo', 'ProdukController@cariMerekLogo')->name('cari.merek.logo');
        Route::post('/cari-data-supplier', 'ProdukController@cariDataSupplier')->name('cari.data.supplier');
        Route::post('/hitung-harga-diskon', 'ProdukController@hitungHargaDiskon')->name('hitung.harga.diskon');    
        Route::post('/cari-gambar-produk', 'ProdukController@cariGambarProduk')->name('cari.gambar.produk');   
});

Route::get('/desain-email-tagihan', function() {
    return view('email.desain-tagihan');
});

Route::get('/format-uang', function() {
    return view('blank');
});

Route::get('/blank', function() {
    return view('blank');
});


Route::get('/pusher', function() {
    return view('pusher');
});

Route::get('test', function () {
    event(new App\Events\StatusLiked('Someone'));
    return "Event has been sent!";
});