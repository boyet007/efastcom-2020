function formatAngka(parAngka) {
    var number_string = parAngka.replace(/[^.\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        format    		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
  
    if(ribuan){
        separator = sisa ? ',' : '';
        format += separator + ribuan.join(',');
    }
  
    format = split[1] != undefined ? format + ',' + split[1] : format;
    return format;
  }

function bikinHiddenInput(parElement, parValue) {
    //hapus hidden
    $('input[name=' + parElement + ']').remove();

    $('<input>').attr({
        type: 'hidden', name: parElement, value: parValue
    }).appendTo('form');
}

$('#provinsi_id').change(function(e) {
    e.preventDefault();
    if($(this).val() !== '') {
        var provinsiId = $(this).val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({ 
            url: "/cari-list-kota",
            data: { provinsiId },
            type: "POST",
            success: function(data){
                var listKota = data;
                $('#kabupaten_id').empty();
                $('<option />', {
                    val: '',
                    text: '--- Pilih Kabupaten / Kota ---'
                }).appendTo('#kabupaten_id');
                
                $(listKota).each(function () {
                    $("<option />", {
                        val: this.city_id,
                        text: this.city_name
                    }).appendTo('#kabupaten_id');
                });
                $('#kode_pos').val('');

            },
            error: function(error) {
                console.log(error);
            }
        });
    }
});

$('#kabupaten_id').change(function(e) {
    e.preventDefault();
    if($(this).val() !== '') {
        var kotaId = $(this).val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({ 
            url: "/cari-provinsi-id",
            data: { kotaId },
            type: "POST",
            success: function(data){
                var kota = data;
                $('#provinsi_id').val(kota['province_id']);
                $('#kode_pos').val(kota['postal_code']);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }
});

$('#tutup, #tutup_2').click(function(e) {
    e.preventDefault();
    $('.livechatbanner').css({ 'display' : 'none' });
    $('.livechat').css({ 'display' : 'block' });

});

$('.livechat').click(function(e) {
    e.preventDefault();
    $(this).css({ 'display' : 'none' });
    $('#chatbox').css({ 'display' : 'block'});
});


//live chat 
$(document).on('click', '.panel-heading span#minim_chat_window', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.removeClass('fa-window-minimize').addClass('fa-plus');
        $this.addClass('panel-collapsed');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('fa-plus').addClass('fa-window-minimize');
        $this.removeClass('panel-collapsed');
    }
});

$(document).on('focus', '.panel-footer input.chat_input', function (e) {
    var $this = $(this);
    if ($('#minim_chat_window').hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideDown();
        $('#minim_chat_window').removeClass('panel-collapsed');
        $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('click', '#new_chat', function (e) {
    var size = $( ".chat-window:last-child" ).css("margin-left");
     size_total = parseInt(size) + 400;
    alert(size_total);
    var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
    clone.css("margin-left", size_total);
});
$(document).on('click', '.fa-window-close', function (e) {
    //$(this).parent().parent().parent().parent().remove();
    $( "#chatbox" ).hide();
    $('.livechat').css({ 'display' : 'block' });
});

// send function start

function send(){
	var chat = $("#btn-input").val(); 
var dt = new Date();
var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

if (chat =="") {
    alert('Enter Message');
} else
{
var body =                       '<div class="row msg_container base_sent">' +
						'<div class="col-md-10 col-xs-10 ">' +
                            '<div class="messages msg_sent">' +
                                '<p>'+ chat + '</p>'+
                               ' <time datetime="2009-11-13T20:00">Administrator • Today '+time+'</time>'+
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-xs-2 avatar">' +
                            '<img class="chatimg" src="https://cheme.mit.edu/wp-content/uploads/2017/01/stephanopoulosgeorge-431x400.jpg" class=" img-responsive ">' +
                        '</div>' +
					'</div>';
}
$(body).appendTo("#messagebody");
$('#btn-input').val('');
$("#messagebody").animate({ scrollTop: $("#messagebody")[0].scrollHeight}, 'slow');
}


// send function end




$( "#btn-chat" ).click(function() {
send()
});