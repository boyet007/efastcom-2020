@extends('layouts.web')

@section('content')
<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">cekout</h3>
						<ul class="breadcrumb-tree">
							<li><a href="{{ url('/') }}">Beranda</a></li>
							<li class="active">cekout</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
        </div>
        
        <div class="section">
			<!-- container -->
			<div class="container cekout">
				<!-- row -->
				<div class="row">
					{!! Form::open(['route' => 'proses.pesanan', 'method' => 'POST', 
						'novalidate' => 'novalidate']) !!}
						{!! Form::hidden('user_id', Auth::user()->id) !!}
						<div class="col-md-7">
							<!-- Billing Details -->
							<div class="billing-details">
								<div class="section-title">
									<h3 class="title">Data Pelanggan</h3>
								</div>
								<div class="form-group">
									{!! Form::text('email', Auth::user()->email, ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('nama_lengkap', Auth::user()->nama_lengkap, ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::textarea('alamat', Auth::user()->alamat, ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('kota', $dataKota['city_name'], ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('propinsi', $dataKota['province'], ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('kode_pos', $dataKota['postal_code'], ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
								<div class="form-group">
									{!! Form::text('telpon', Auth::user()->no_telpon, ['class' => 'input', 'disabled' => 'disabled']) !!}
								</div>
							</div>
							<!-- /Billing Details -->

							<!-- Order notes -->
							<div class="order-notes">
								<textarea class="input" name="catatan" placeholder="Catatan"></textarea>
							</div>
							<!-- /Order notes -->
						</div>

						<!-- Order Details -->
						<div class="col-md-5 order-details">
							<div class="section-title text-center">
								<h3 class="title">List Order Anda</h3>
							</div>
							<div class="order-summary">
								<div class="order-col">
									<?php
										$total_harga = 0;
										$total_berat = 0;
										
										foreach($keranjang as $k) {
											$total_harga += ($k['produk_harga'] * $k['qty']);
											$total_berat += ($k['produk_berat'] * $k['qty']) / 1000;		
										}
									?>
									<div><strong>PRODUK</strong></div>
									<div><strong>TOTAL</strong></div>
								</div>
								<div class="order-products">
									@foreach($keranjang as $k)
										<div class="order-col">
											<div>{{ $k['qty'] }} x {{ $k['produk_nama'] }}</div>
											<div>Rp {{ formatUang($k['produk_harga']) }},00</div>
										</div>
									@endforeach
								</div>
								<div class="order-col">
									<div><strong>SUBTOTAL</strong></div>
									<div><strong class="order-total">Rp {{ formatUang($total_harga) }},00</strong></div>
								</div>
								<div class="order-col">
									<div>Total Berat</div>
									<div>{{ $total_berat }} kg</div>
								</div>
								<div class="order-col">
									<div>Kurir</div>
									<div>
										<select name="kurir" id="kurir" class="order-col">
											<option value="">--- Pilih Kurir ---</option>
											@foreach ($arrKurir as $key => $value )
												<option value="{{ $key }}" @if(old('kurir') == $key) selected @endif>
													{{ $value }}</option>
											@endforeach
										</select>
										@if($errors->has('kurir'))
											<span class="help-block">{{ $errors->first('kurir') }}</span>
										@endif
									</div>
								</div>
								<div class="order-col">
									<div>Paket</div>
									<div>
										<select name="paket" id="paket" disabled="disabled" class="order-col">
											<option value="">--- Pilih Paket ---</option>
										</select>
										@if($errors->has('paket'))
											<span class="help-block">{{ $errors->first('paket') }}</span>
										@endif
									</div>
								</div>
								<div class="order-col">
									<div>Estimasi</div>
									<div id="estimasi">hari</div>
								</div>
								<div class="order-col">
									<div>Ongkos Kirim</div>
									<div id="shipping">0</div>
								</div>

							<div class="order-col">
									<div><strong>TOTAL</strong></div>
									<div id="order-total"><strong class="order-total">Rp {{ formatUang($total_harga) }},00</strong></div>
								</div>
							</div>
							{!! Form::submit('Pesan', ['class' => 'primary-btn order-submit']) !!}	
						</div>
						<!-- /Order Details -->
						<div class="col-md-offset-7 col-md-5 order-details">
							<div class="section-title text-center">
								<h3 class="title">Pembayaran</h3>
							</div>
							<p>Customer harap menyelesaikan pembayaran terlebih dahulu dengan mentransfer ke bank <b>{{ config('info.EFASTCOM_BANK') }}</b> 
							no rekening <b>{{ config('info.EFASTCOM_NOREK') }}</b> atas nama <b>{{ config('info.EFASTCOM_NAMA_BANK') }}</b></p>
						</div>
					{!! Form::close() !!}
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
@stop

@push('scripts')
	<script>
		var subTotal = {!! json_encode($total_harga) !!}
		var jsonOngkir;

		function bikinHiddenInput(parElement, parValue) {
			//hapus hidden
			$('input[name=' + parElement + ']').remove();

			$('<input>').attr({
				type: 'hidden', name: parElement, value: parValue
			}).appendTo('form');
		}

		function formatAngka(parAngka) {
	
			var	number_string = parAngka.toString(),
				sisa 	= number_string.length % 3,
				rupiah 	= number_string.substr(0, sisa),
				ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
					
			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			return rupiah;
		}
		
		$('#kurir').change(function(e) {
			e.preventDefault();
			var kurir = $(this).val();
			if(kurir) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({ 
					url: "/hitung-biaya-kirim",
					data: { kurir },
					type: "POST",
					success: function(data){
		
						jsonOngkir = JSON.parse(data);
						$('#paket').empty()

						for(var i=0; i < jsonOngkir.length;i++)
						{
							jQuery('<option/>', {
								value: jsonOngkir[i].service,
								html: jsonOngkir[i].service
							}).appendTo('#paket'); 
						}

						$("#paket").prepend("<option value='' selected='selected'>--- Pilih Paket ---</option>");
						$("#paket").prop("disabled", false);

						bikinHiddenInput('kurir', kurir);

					},
					error: function(error) {
						console.log(error);
					}
				});
			} else {

				ongkir = 0;
				total = subTotal + ongkir;
				$('#paket').val('').prop('disabled', true);
				$('#estimasi').text('hari');  
				$('#shipping').text('0');
				$('#order-total').html('<strong class="order-total">Rp ' + formatAngka(total) + ",00</strong>");
				$('input[name="kurir"]').remove();
			}
		});

		$('#paket').change(function(e) {
			e.preventDefault();
			var ongkir;
			var total;
			var kurir = $('#kurir').val();
			var paket = $(this).val();
			if(kurir && paket) { 
				const filterOngkir = jsonOngkir.filter(o => o.service === paket);
				const dataOngkir = filterOngkir[0].cost[0];
				ongkir = dataOngkir.value;
				total = subTotal + ongkir;

				bikinHiddenInput('paket', paket);
				bikinHiddenInput('estimasi', dataOngkir.etd);
				bikinHiddenInput('ongkir', dataOngkir.value);

				$('#estimasi').text(dataOngkir.etd + ' hari');
				$('#shipping').text('Rp ' + formatAngka(dataOngkir.value) + ',00');
				$('#order-total').html('<strong class="order-total">Rp ' + formatAngka(total) + ",00</strong>");
			} else {
				ongkir = 0;
				total = subTotal + ongkir;
				$('#estimasi').text('hari');  
				$('#shipping').text('0');
				$('#order-total').html('<strong class="order-total">Rp ' + formatAngka(total) + ",00</strong>");
				$('input[name="paket"]').remove();
			}
		});

		$(document).ready(function() {
			var kurir = $('#kurir').val();
			if (kurir) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({ 
					url: "/hitung-biaya-kirim",
					data: { kurir },
					type: "POST",
					success: function(data){
						jsonOngkir = JSON.parse(data);
						$('#paket').empty()

						for(var i=0; i < jsonOngkir.length;i++)
						{
							jQuery('<option/>', {
								value: jsonOngkir[i].service,
								html: jsonOngkir[i].service
							}).appendTo('#paket'); 
						}

						$("#paket").prepend("<option value='' selected='selected'>--- Pilih Paket ---</option>");
						$("#paket").prop("disabled", false);

						bikinHiddenInput('kurir', kurir);

					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		});
	</script>
@endpush

