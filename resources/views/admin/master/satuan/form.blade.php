{!! Form::model($model, [
    'route' => $model->exists ? ['satuan.update', $model->id] : 'satuan.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}


<div class="row">
    <div class="col-6">
        <div class="form-group">
            @if($model->exists)
                {!! Form::hidden('kode', $model->kode); !!}
                {!! Form::text('kode', null, ['disabled' => 'disabled', 'class' => 'form-control', 'id' => 'kode', 'placeholder' => 'Masukkan kode Satuan']) !!}
            @else 
                {!! Form::text('kode', null, ['class' => 'form-control', 'id' => 'kode', 'placeholder' => 'Masukkan kode Satuan']) !!}
            @endif
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            {!! Form::text('nama', null, ['class' => 'form-control', 'id' => 'nama', 'placeholder' => 'Masukkan Nama Satuan']) !!}
        </div>
    </div>
</div>

{!! Form::close() !!}