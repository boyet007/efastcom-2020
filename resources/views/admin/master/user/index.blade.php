@extends('layouts.admin')

@push('styles')
    <style>
        #datatable td:nth-child(2) {
            width:60%;
        }
    </style>
@endpush

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="card-body">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="card shadow">
                    <div class="card-header bg-warning py-3">
                      <h2 class="font-weight-bold float-left">{{ $sub_title }}</h2>
                    </div>
                    <div class="card-body">
                        <table id="datatable" class="table table-hover table-user">
                            <thead>
                                <th>EMAIL</th>
                                <th>NAMA LENGKAP</th>
                                <th>NO_TELP</th>
                                <th>ROLE</th>
                                <th>AKSI</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('table.user') }}",
            columns: [
                { data: 'email', name: 'email' },
                { data: 'nama_lengkap', name: 'nama_lengkap' },
                { data: 'no_telpon', name: 'no_telpon' },
                { data: 'role', name: 'role' },
                { data: 'aksi', name: 'aksi' }
            ],
            language: {
                lengthMenu: 'Tampil _MENU_ rekord per halaman',
                info: 'Tampil hal _PAGE_ dari _PAGES_',
                processing: 'Sedang memproses...',
                search: 'Cari',
                zeroRecords: 'Tidak ditemukan rekord',
                paginate: {
                    first: 'Pertama',
                    last: 'Terakir',
                    next: 'Selanjutnya',
                    previous: 'Sebelumnya',
                },
                infoFiltered: "( filter dari _MAX_ jumlah rekord )"
            }
            })
        })
    </script>
@endpush