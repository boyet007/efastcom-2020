<table class="table table-borderless master-show">
    <tbody>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $model->email }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $model->nama_lengkap }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{ $model->alamat }}</td>
        </tr>
        <tr>
            <td>Kabupaten / Kota</td>
            <td>:</td>
            <td>{{ $model->kabupaten() }}</td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td>{{ $model->provinsi() }}</td>
        </tr>
        <tr>
            <td>Kode Pos</td>
            <td>:</td>
            <td>{{ $model->kode_pos() }}</td>
        </tr>
        <tr>
            <td>No Telpon</td>
            <td>:</td>
            <td>{{ $model->no_telpon }}</td>
        </tr>
        <tr>
            <td>Role</td>
            <td>:</td>
            <td>{{ $model->role }}</td>
        </tr>
        <tr>
            <td>Status Verifikasi</td>
            <td>:</td>
            <td>{{ $model->verifikasi() }}</td>
        </tr>


    </tbody>
</table>