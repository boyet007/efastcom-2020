@extends('layouts.admin')

@push('styles')
    <style>
        #datatable td:nth-child(2) {
            width:30%;
        }
    </style>
@endpush

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        @if (session('error'))
            <div class="alert alert-error">
                {{ session('error') }}
            </div>
        @endif
        <div class="card-body">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-7">
                <div class="card shadow">
                    <div class="card-header bg-warning py-3">
                      <h2 class="font-weight-bold float-left">{{ $sub_title }}</h2>
                      <a href="{{ route('kategori-produk.create') }}" data-toggle="modal" tipe="tambah" 
                            judul='{{ $form_title }}' 
                            data-backdrop="static" class="btn btn-dark float-right mt-2 btn-sm btn-tampil-modal">
                             <i class="fa fa-plus"></i></a>
                    </div>
                    <div class="card-body">
                        <table id="datatable" class="table table-hover table-kategori-produk">
                            <thead>
                                <th width="10">ID</th>
                                <th>GAMBAR</th>
                                <th>NAMA</th>
                                <th width="50">AKSI</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({
            serverSide: true,
            processing: true,
            responsive: true,
            ajax: "{{ route('table.kategori.produk') }}",
            columns: [
                { data: 'id', name: 'id' },
                { data: 'gambar', name: 'produk.gambar', searchable: false, render:function(data,tipe, row) {
                    return "<img src='/web/img/" + data + "'>";
                }, orderable: false },   
                { data: 'nama', name: 'nama' },
                { data: 'aksi', name: 'aksi' }
            ],
            language: {
                lengthMenu: 'Tampil _MENU_ rekord per halaman',
                info: 'Tampil hal _PAGE_ dari _PAGES_',
                processing: 'Sedang memproses...',
                search: 'Cari',
                zeroRecords: 'Tidak ditemukan rekord',
                paginate: {
                    first: 'Pertama',
                    last: 'Terakir',
                    next: 'Selanjutnya',
                    previous: 'Sebelumnya',
                },
                infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            
            $('body').on('change', '#file', function() {
                bacaUrl(this)
            });
        })
    </script>
@endpush