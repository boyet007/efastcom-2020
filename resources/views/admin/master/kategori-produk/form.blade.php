{!! Form::model($model, [
    'route' => $model->exists ? ['kategori-produk.update', $model->id] : 'kategori-produk.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'files' => 'true'
]) !!}

@if($model->gambar)
{{ Form::hidden('nama_file_lama', $model->gambar) }}
@endif

<div class="card mb-2">
    <div class="card-header bg-success">
        <h6 class="text-white font-weight-bold">Input Kategori Produk</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-12">
                <div class="form-group">
                    {!! Form::text('nama',  null, ['id' => 'nama', 'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {{ Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm'])}}
                </div>
                <div class="row">
                    <div class="col-12">
                        @if($model->gambar)
                            <img id="data_gambar" src="{{ url('/web/img/' . $model->gambar) }}" />    
                        @else
                            <img id="data_gambar" src="{{ url('/web/img/unknown.jpg') }}" />
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}