<table class="table table-borderless master-show">
    <tbody>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $model->nama }}</td>
        </tr>
        <tr class="gambar">
            <td>Gambar</td>
            <td>:</td>
            <td><img src="{{ url('/web/img/' . $model->gambar)  }}" /></td>
        </tr>
    </tbody>
</table>