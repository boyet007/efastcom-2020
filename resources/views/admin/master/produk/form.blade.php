{!! Form::model($model, [
    'route' => $model->exists ? ['admin-produk.update', $model->id] : 'admin-produk.store',
    'method' => $model->exists ? 'PUT' : 'POST',
    'novalidate' => 'novalidate',
    'files' => 'true'
]) !!}

<div class="card mb-2">
    <div class="card-header bg-success">
        <h6 class="text-white font-weight-bold">Input Data Produk</h6></div>

    <div class="card-body">
        {{ Form::hidden('user_id', Auth::user()->id )}}

        <div class="form-row">
            <div class="col-6">
                <div class="form-group">
                    {!! Form::select('kategori_id', $listKategoriProduk, null, ['id' => 'kategori_id', 
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('nama', null, ['class' => 'form-control form-control-sm', 'id' => 'nama', 
                        'placeholder' => 'Masukkan Nama Produk']) !!}
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            {!! Form::number('qty', null, ['class' => 'form-control form-control-sm', 'id' => 'qty', 
                            'min' => 0, 'placeholder' => 'Quantity']) !!}
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            {!! Form::select('satuan_id', $listSatuan, null, ['id' => 'satuan_id', 
                            'class' => 'form-control form-control-sm']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            {!! Form::text('harga', null, ['class' => 'form-control form-control-sm', 
                            'id' => 'harga', 
                            'placeholder' => 'Harga Jual']) !!}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group input-group-sm mb-3">
                            {!! Form::number('diskon', null, ['id' => 'diskon', 'min' => 0, 'max' => 100,
                            'class' => 'form-control form-control-sm', 'placeholder' => 'Diskon']) !!}
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            {!! Form::text('harga_beli', null, ['class' => 'form-control form-control-sm', 'id' => 'harga_beli', 
                            'placeholder' => 'Harga Beli']) !!}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            {!! Form::date('tanggal_beli', null, ['id' => 'tanggal_beli', 
                            'class' => 'form-control form-control-sm', 'placeholder' => 'Tanggal_Beli']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::select('merek_id', $listMerek, null, ['id' => 'merek_id', 
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {!! Form::select('supplier_id', $listSupplier, null, ['id' => 'supplier_id', 
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {!! Form::textarea('ket', null, ['id' => 'ket', 'rows' => '3', 'placeholder' => 'Masukkan Keterangan (boleh dikosongkan)', 
                        'class' => 'form-control form-control-sm']) !!}
                </div>
                <div class="form-group">
                    {{ Form::file('file', ['id' => 'file', 'class' => 'form-control form-control-sm'])}}
                </div>
                @if($model->exists)
                    {{ Form::hidden('nama_file_lama', $model->gambar) }}
                @endif
                <div class="card">
                    <div class="card-header bg-warning">
                        <h6 class="font-weight-bold">Gambar Produk</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <img id="data_gambar" src="{{ url('/web/img/produk/unknown.jpg') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6 info">
                <div class="card">
                    <div class="card-header bg-info">
                        <h6 class="text-white font-weight-bold">Data Produk</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Satuan</label></div>
                            <div class="col-8">{!! Form::text('data_satuan_nama', null, ['class' => 'form-control form-control-sm', 'disabled' => 'disabled', 
                                'id' => 'data_satuan_nama', 'placeholder' => 'Nama Satuan']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Harga Akhir</label></div>
                            <div class="col-8">{!! Form::text('data_harga_diskon', null, ['class' => 'form-control form-control-sm', 'disabled' => 'disabled', 
                                'id' => 'data_harga_diskon', 'placeholder' => 'Harga Jual']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Merek</label></div>
                            <div class="col-8"><img id="data_gambar_merek" src="{{ url('/web/img/unknown.jpg') }}" /></div>
                        </div>
                    </div>
                </div>
                
                <div class="card mt-2 info">
                    <div class="card-header bg-info">
                        <h6 class="text-white font-weight-bold">Data Supplier</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Alamat</label></div>
                            <div class="col-8">{!! Form::textarea('data_supplier_alamat', null, ['class' => 'form-control form-control-sm', 
                                'rows' => '3', 'disabled' => 'disabled', 
                                'id' => 'data_supplier_alamat', 'placeholder' => 'Alamat Supplier']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Kota</label></div>
                            <div class="col-8">{!! Form::text('data_supplier_kota', null, ['class' => 'form-control form-control-sm', 'disabled' => 'disabled', 
                                'id' => 'data_supplier_kota', 'placeholder' => 'Kota Supplier']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">PIC</label></div>
                            <div class="col-8">{!! Form::text('data_supplier_pic', null, ['class' => 'form-control form-control-sm', 'disabled' => 'disabled', 
                                'id' => 'data_supplier_pic', 'placeholder' => 'PIC Supplier']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="col-4"><label class="label col-form-label-sm">Telpon</label></div>
                            <div class="col-8">{!! Form::text('data_supplier_telpon', null, ['class' => 'form-control form-control-sm', 'disabled' => 'disabled', 
                                'id' => 'data_supplier_telpon', 'placeholder' => 'Telpon Supplier']) !!}</div>
                        </div>
                    </div>
                </div>
                @if($model->exists )
                    {!! Form::hidden('hidden_harga', $model->harga) !!}
                    {!! Form::hidden('hidden_harga_beli', $model->harga_beli) !!}
                @endif

            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

