<table class="table table-borderless master-show">
    <tbody>
        <tr>
            <td>ID</td>
            <td>:</td>
            <td>{{ $model->id }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $model->nama }}</td>
        </tr>
        <tr>
            <td>Gambar</td>
            <td>:</td>
        <td><img src="{{ url('/web/img/' . $model->gambar) }}"</td>
        </tr>
    </tbody>
</table>