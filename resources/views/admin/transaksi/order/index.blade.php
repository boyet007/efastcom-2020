@extends('layouts.admin')

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="card-body">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="card shadow">
                    <div class="card-header bg-warning py-3">
                      <h2 class="font-weight-bold float-left">{{ $sub_title }}</h2>
                      <a href="{{ route('admin-produk.create') }}" data-toggle="modal" tipe="tambah" 
                            judul='{{ $form_title }}' ukuran='besar' 
                            data-backdrop="static" class="btn btn-dark float-right mt-2 btn-sm btn-tampil-modal">
                             <i class="fa fa-plus"></i></a>
                    </div>
                    <div class="card-body">
                        <table id="datatable" class="table table-hover table-produk">
                            <thead>
                                <th>ID</th>
                                <th>CUSTOMER</th>
                                <th>KURIR</th>
                                <th>SUBTOTAL</th>
                                <th>ONGKIR</th>
                                <th>GRANDTOTAL</th>
                                <th>CATATAN</th>
                                <th>TANGGAL BELI</th>
                                <th>AKSI</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>      
        function prosesHargaDiskon() {
            var harga = $('#harga').val();
            var diskon = $('#diskon').val();
            var parHargaNonFormat = accounting.unformat(harga); 
            if (harga.trim() === '') { harga = 0; }
            if (diskon.trim() === '') { diskon = 0; }
            
            var hargaDiskon = hitungDiskon(parHargaNonFormat, diskon);

            if (hargaDiskon === 0) {
                $('#data_harga_diskon').val(0);
            } else {
                $('#data_harga_diskon').val(accounting.formatMoney(hargaDiskon, 'Rp ', 2, ',', '.'));    
            }

            return parHargaNonFormat;
        }

        $(document).ready(function() {
            $('#datatable').DataTable({
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: "{{ route('table.order') }}",
                columns: [
                    { data: 'id', name: 'order.id' },
                    { data: 'nama', name: 'user.nama_lengkap' },
                    { data: 'kurir', name: 'order.kurir' }, 
                    { data: 'subtotal', name: 'order.subtotal' },  
                    { data: 'grantotal', name: 'order.grantotal' },
                    { data: 'catatan', name: 'order.catatan' },    
                    { data: 'aksi', name: 'aksi', orderable: false }
                ],
                language: {
                    lengthMenu: 'Tampil _MENU_ rekord per halaman',
                    info: 'Tampil hal _PAGE_ dari _PAGES_',
                    processing: 'Sedang memproses...',
                    search: 'Cari',
                    zeroRecords: 'Tidak ditemukan rekord',
                    paginate: {
                        first: 'Pertama',
                        last: 'Terakir',
                        next: 'Selanjutnya',
                        previous: 'Sebelumnya',
                    },
                    infoFiltered: "( filter dari _MAX_ jumlah rekord )"
                }
            })

            $('body').on('change', '#satuan_id', function(event){
                event.preventDefault();
                
                if($(this).val() !== '' ) {
                    var satuanId = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({ 
                        url: "/cari-nama-satuan",
                        data: { satuanId },
                        type: "POST",
                        success: function(data){
                            var namaSatuan = data;
                            $('#data_satuan_nama').val(namaSatuan);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } else {
                    $('#data_satuan_nama').val('');
                }
            });

            $('body').on('keyup', '#harga', function(event){
                event.preventDefault();
                var hargaNonFormat = prosesHargaDiskon();               
                $('#harga').val(formatAngka($(this).val())); 
                bikinHiddenInput('hidden_harga', hargaNonFormat);
            });

            $('body').on('keyup', '#diskon', function(event){
                event.preventDefault();
                prosesHargaDiskon();               
            });

            $('body').on('keyup', '#harga_beli', function(event){
                event.preventDefault();
                $('#harga_beli').val(formatAngka($(this).val())); 

                //bikin hidden value
                var integerHarga = accounting.unformat($(this).val()); 
                bikinHiddenInput('hidden_harga_beli', integerHarga);
            });

            $('body').on('change', '#merek_id', function(event){
                event.preventDefault();
                
                if($(this).val() !== '' ) {
                    var merekId = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({ 
                        url: "/cari-merek-logo",
                        data: { merekId },
                        type: "POST",
                        success: function(data){
                            var gambar = data;
                            $('#data_gambar_merek').attr('src', 'web/img/' + gambar);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } else {
                    $('#data_gambar_merek').attr('src', 'web/img/unknown.jpg');
                }
            });

            $('body').on('change', '#supplier_id', function(event){
                event.preventDefault();
                
                if($(this).val() !== '' ) {
                    var supplierId = $(this).val();

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({ 
                        url: "/cari-data-supplier",
                        data: { supplierId },
                        type: "POST",
                        success: function(data){
                            $('#data_supplier_alamat').val(data.alamat);
                            $('#data_supplier_kota').val(data.kota.nama);
                            $('#data_supplier_pic').val(data.pic);
                            $('#data_supplier_telpon').val(data.no_telpon);
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                } else {
                    $('#data_supplier_alamat').val('');
                    $('#data_supplier_kota').val('');
                    $('#data_supplier_pic').val('');
                    $('#data_supplier_telpon').val('');
                }
            });

            $('body').on('change', '#file', function() {
                bacaUrl(this)
            });

            $('#modal').on('shown.bs.modal', function(e){
                var status = $('#modal').attr('tipe');
                if(status === 'edit') {
                    var produkId = $('#modal').attr('kode');
                    var satuanId;
                    var merekId;
                    var supplierId;
                    var harga;
                    var diskon;

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    //cari data produk
                    $.ajax({ 
                        url: '/cari-data-produk',
                        data: { produkId},
                        async: false,
                        type: "POST",
                        success: function(data){
                            var produk = data;
                            id = produk.id;
                            satuanId = produk.satuan_id;
                            merekId = produk.merek_id;
                            supplierId = produk.supplier_id;
                            harga = produk.harga;
                            harga_beli = produk.harga_beli;
                            gambar = produk.gambar;

                            if(produk.diskon === null) {
                                diskon = 0;
                            } else {
                                diskon = produk.diskon;
                            }

                            $.ajax({ 
                                url: '/cari-nama-satuan',
                                data: { satuanId },
                                type: "POST",
                                success: function(res){
                                    $('#data_satuan_nama').val(res);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });
                            
                            $.ajax({ 
                                url: '/hitung-harga-diskon',
                                data: { harga, diskon },
                                type: "POST",
                                success: function(res){
                                    var hargaDiskon = res;
                                    $('#data_harga_diskon').val(accounting.formatMoney(hargaDiskon, 'Rp ', 2, ',', '.'));  
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-merek-logo",
                                data: { merekId },
                                type: "POST",
                                success: function(data){
                                    var gambar = data;
                                    $('#data_gambar_merek').attr('src', 'web/img/' + gambar);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-data-supplier",
                                data: { supplierId },
                                type: "POST",
                                success: function(data){
                                    $('#data_supplier_alamat').val(data.alamat);
                                    $('#data_supplier_kota').val(data.kota.nama);
                                    $('#data_supplier_pic').val(data.pic);
                                    $('#data_supplier_telpon').val(data.no_telpon);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            $.ajax({ 
                                url: "/cari-gambar-produk",
                                data: { id },
                                type: "POST",
                                success: function(data){
                                    var gambar = data;
                                    $('#data_gambar').attr('src', 'web/img/produk/' + gambar);
                                },
                                error: function(error) {
                                    console.log(error);
                                }
                            });

                            //bikin hidden input untuk harga dan harga beli
                            // bikinHiddenInput('hidden_harga', harga);
                            // bikinHiddenInput('hidden_harga_beli', harga_beli);
                            
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    }); 
                }
            });
        })
    </script>
@endpush