@extends('layouts.web')

@section('content')

<div id="breadcrumb" class="section">

		<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumb-tree">
							<li><a href="/">Beranda</a></li>
							<?php 
								if(app('request')->input('kategori_id')) {
									$kategori_id = app('request')->input('kategori_id');		
								} else if (Request::segment(2)) {
									$kategori_id = Request::segment(2);
								}
							?> 
							@if(isset($kategori_id))
								<li class="active"><a href="{{ url('/web-kategori-produk/' . $kategori_id) }}">{{ App\KategoriProduk::find($kategori_id)->nama }}</a></li>
							@endif						
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
        </div>
      
        <div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- STORE -->
					<div id="store" class="col-md-12">
						<!-- store products -->
						<div class="row">

							<div class="clearfix"></div>
							<!-- product -->
							@if($produk->count() > 0)
								@foreach($produk as $p) 
								<div class="col-md-3 col-xs-6">
									<div class="product">
										<div class="product-img">
											<img src="{{ asset('web/img/produk/' . $p->gambar) }}" alt="">
											<div class="product-label">
												@if($p->diskon > 0)
													<span class="sale">{{ $p->diskon }} %
													</span>
												@endif

												<!-- barang baru kurang dari seminggu -->
												@if(hitungUsiaProduk($p->tanggal_beli) < config('info.LIMIT_PRODUK_BARU'))
													<span class="new">BARU</span>
												@endif
											</div>
										</div>
										<div class="product-body">
											<p class="product-category">{{ $p->kategori_produk->nama }}</p>
											<h3 class="product-name"><a href="{{ url('/produk/' . $p->id) }}">{{ $p->nama }}</a></h3>
											@if(isset($p->diskon))
												<h4 class="product-price">Rp {{ formatUang($p->hargaSetelahDiskon()) }},00
													<del class="product-old-price">Rp {{ formatUang($p->harga) }},00</del>
												</h4>
											@else 
												<h4 class="product-price">Rp {{ formatUang($p->harga) }},00</h4>
											@endif
										</div>
										<div class="add-to-cart">
											{!! Form::open(['route' => 'tambah.keranjang', 'method' => 'POST', 
											'novalidate' => 'novalidate']) !!}
												{!! Form::hidden('qty', '1') !!}
												{!! Form::hidden('produk_id', $p->id) !!}
												{!! Form::submit('Tambah Ke Keranjang', ['class' => 'add-to-cart-btn']) !!}
											{!! Form::close() !!}
										</div>
									</div>
								</div>
								@endforeach
							@else 
								<div class="col-md-12">
									<h4>Maaf barang yang anda cari tidak ada.</h4>
								</div>
							@endif

							<!-- /product -->

						</div>
						<!-- /store products -->

						<!-- store bottom filter -->
						<div class="store-filter clearfix">
							{{ $produk->links() }}
						</div>
						<!-- /store bottom filter -->
					</div>
					<!-- /STORE -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>

@stop
