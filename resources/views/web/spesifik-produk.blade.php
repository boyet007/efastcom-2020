@extends('layouts.web')

@section('content')
<div id="breadcrumb" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<ul class="breadcrumb-tree">
							<li><a href="#">Beranda</a></li>
							<li><a href="#">{{ $produk->kategori_produk->nama }}</a></li>
							<li class="active">{{ $produk->nama }}</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
        </div>
        
        <div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Product main img -->
					<div class="col-md-5">
						<div id="product-main-img" class="slick-initialized slick-slider">
							<div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 1832px;"><div class="product-preview slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 458px; position: relative; left: 0px; top: 0px; z-index: 999; opacity: 1; overflow: hidden;">
								<img src="{{ url('/web/img/produk/' . $produk->gambar) }}" alt="">
							<img role="presentation" src="{{ asset('web/img/product01.png') }}" class="zoomImg" style="position: absolute; top: -4.65066px; left: -5.42576px; opacity: 0; width: 600px; height: 600px; border: none; max-width: none; max-height: none;"></div><div class="product-preview slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" style="width: 458px; position: relative; left: -458px; top: 0px; z-index: 998; opacity: 0; overflow: hidden;">
								<img src="{{ asset('web/img/product03.png') }}" alt="">
                            <img role="presentation" src="{{ asset('web/img/product03.png') }}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 600px; height: 600px; border: none; max-width: none; max-height: none;"></div><div class="product-preview slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 458px; position: relative; left: -916px; top: 0px; z-index: 998; opacity: 0; overflow: hidden;">
								<img src="{{ asset('web/img/product06.png') }}" alt="">
                            <img role="presentation" src="{{ asset('web/img/product06.png') }}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 600px; height: 600px; border: none; max-width: none; max-height: none;"></div><div class="product-preview slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" style="width: 458px; position: relative; left: -1374px; top: 0px; z-index: 998; opacity: 0; overflow: hidden;">
								<img src="{{ asset('web/img/product08.png') }}" alt="">
                            <img role="presentation" src="{{ asset('web/img/product08.png') }}" class="zoomImg" style="position: absolute; top: 0px; left: 0px; opacity: 0; width: 600px; height: 600px; border: none; max-width: none; max-height: none;"></div></div></div>
						</div>
					</div>
					<!-- /Product main img -->

					<!-- Product details -->
					<div class="col-md-5">
						<div class="product-details">
							<h2 class="product-name">{{ $produk->nama }}</h2>
							<div>
								@if(isset($produk->diskon))
									<h3 class="product-price">Rp {{ format_uang($produk->hargaSetelahDiskon()) }},00 <del class="product-old-price">Rp {{ format_uang($produk->harga) }},00</del></h3>
								@else
									<h3 class="product-price">Rp {{ format_uang($produk->harga) }},00</h3>
								@endif

								@if($produk->qty > 0)
									<span class="product-available">Stok {{ $produk->qty }} {{ $produk->satuan->kode }}</span>
								@else 
									<span class="product-unavailable">Stok Habis</span>
								@endif
							</div>
							<p>{{ $produk->ket }}</p>

							<div class="add-to-cart">
								{!! Form::open(['route' => 'tambah.keranjang', 'method' => 'POST', 
								'novalidate' => 'novalidate']) !!}
								<div class="qty-label">
									Qty&nbsp;{!! Form::number('qty', 1, ['class' => 'input-number',
									'min' => '0']) !!}
								</div>
									{!! Form::hidden('produk_id', $produk->id) !!}
									{!! Form::submit('Tambah Ke Keranjang', ['class' => 'add-to-cart-btn']) !!}
								{!! Form::close() !!}
							</div>

							<ul class="product-links">
								<li>Kategori:</li>
								<li>{{ $produk->kategori_produk->nama }}</a></li>
							</ul>
						</div>
					</div>
					<!-- /Product details -->

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
        </div>
        
        <div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					@if($produkLain->count() > 0)
						<div class="col-md-12">
							<div class="section-title text-center">
								<h3 class="title">Produk lain</h3>
							</div>
						</div>

						@foreach($produkLain as $p)
						<!-- product -->
						<div class="col-md-3 col-xs-6">
							<div class="product">
								<div class="product-img">
									<img src="{{ url('/web/img/produk/' . $p->gambar) }}" alt="">
									<div class="product-label">
										@if($p->diskon > 0)
											<span class="sale">{{ $p->diskon }}%</span>
										@endif
										<?php
											$tgl1 = new DateTime($p->tanggal_beli);
											$tgl2 = new DateTime(now());
											$d = $tgl2->diff($tgl1)->days + 1;
										?>
										<!-- barang baru kurang dari seminggu -->
										@if($d < config('info.LIMIT_PRODUK_BARU'))
											<span class="new">BARU</span>
										@endif
									</div>
								</div>
								<div class="product-body">
									<p class="product-category">{{ $p->kategori_produk->nama }}</p>
									<h3 class="product-name"><a href="{{ url('/produk/' . $p->id) }}">{{ $p->nama }}</a></h3>
									@if(isset($p->diskon))
												<h4 class="product-price">Rp {{ format_uang($p->hargaSetelahDiskon()) }},00
													<del class="product-old-price">Rp {{ format_uang($p->harga) }},00</del>
												</h4>
											@else 
												<h4 class="product-price">Rp {{ format_uang($p->harga) }},00</h4>
											@endif
								</div>
							</div>
						</div>
						<!-- /product -->
						@endforeach
					@endif
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
@stop