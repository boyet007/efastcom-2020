<div class="livesupport-btn hidden-sm hidden-xs">
    <div class="livechatbanner ">
        <a class="livesupportclose" title="Tutup LiveChat"><i id="tutup" class="fa fa-times"></i></a>
        <div class="bubblespeech">
            <a href="#" target="_blank" title="Start LiveChat Now" class="btn livechat"></a>
        </div>
    </div> 
    <div class="livechat-btn">
        <a href="#" class="livechat"><span><i class="fa fa-comments-o" style="font-size:25px;"></i></span> Mulai Ngobrol</a> </div> 
    </div>
</div>

<div class="container pull-right hidden-sm hidden-xs" id="chatbox">
    <div class="row chat-window col-xs-5 col-md-3 pull-right" id="chat_window_1">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="col-md-8 col-xs-8">
                        <h3 class="panel-title"><span class="fa fa-comment"></span> Chat</h3>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right;">
                        <a href="#"><span id="minim_chat_window" class="fa fa-window-minimize"></span></a>
                        <a href="#"><span class="fa fa-window-close" id="tutup_2" data-id="chat_window_1"></span></a>
                    </div>
                </div>
                <div id="messagebody" class="panel-body msg_container_base">
                    <div class="row msg_container base_sent">
                        <div class="col-md-10 col-xs-10">
                            <div class="messages msg_sent">
                                <p>Hi</p>
                                <time datetime="2009-11-13T20:00">10:05:22</time>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('/web/img/unknown2.jpg') }}" class="chatimg img-responsive ">
                        </div>
                    </div>
                    <div class="row msg_container base_receive">
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('/web/img/girl.jpg') }}" class="chatimg img-responsive ">
                        </div>
                        <div class="col-md-10 col-xs-10">
                            <div class="messages msg_receive">
                                <p>Hi sir,</p>
                                <time datetime="2009-11-13T20:00">10:05:28</time>
                            </div>
                        </div>
                    </div>
                    <div class="row msg_container base_receive">
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('/web/img/girl.jpg') }}" class="chatimg img-responsive ">
                        </div>
                        <div class="col-xs-10 col-md-10">
                            <div class="messages msg_receive">
                                <p>How are you?</p>
                                <time datetime="2009-11-13T20:00">10:05:33</time>
                            </div>
                        </div>
                    </div>
                    <div class="row msg_container base_sent">
                        <div class="col-xs-10 col-md-10">
                            <div class="messages msg_sent">
                                <p>I am Fine. Hw about u?</p>
                                <time datetime="2009-11-13T20:00">10:05:38</time>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('web/img/unknown2.jpg') }}" class="chatimg img-responsive ">
                        </div>
                    </div>
                    <div class="row msg_container base_receive">
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('/web/img/girl.jpg') }}" class="chatimg img-responsive ">
                        </div>
                        <div class="col-xs-10 col-md-10">
                            <div class="messages msg_receive">
                                <p>I am also doing fine. See you later sir.</p>
                                <time datetime="2009-11-13T20:00">10:05:41</time>
                            </div>
                        </div>
                    </div>
                    <div class="row msg_container base_sent">
                        <div class="col-md-10 col-xs-10 ">
                            <div class="messages msg_sent">
                                <p>Good, Have a nice day.</p>
                                <time datetime="2009-11-13T20:00">10:05:45</time>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="{{ url('web/img/unknown2.jpg') }}" class="chatimg img-responsive ">
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="input-group">
                    <input id="btn-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." required="required" />
                    <span class="input-group-btn">
                    <button class="btn btn-dark btn-sm" id="btn-chat">Send</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>