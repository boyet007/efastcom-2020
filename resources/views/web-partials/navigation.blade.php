<nav id="navigation">
    <!-- container -->
    <div class="container">
        <!-- responsive-nav -->
        <div id="responsive-nav">
            <!-- NAV -->
            <ul class="main-nav nav navbar-nav">
                <li {{ Request::path() === '/' ? "class=active" : null }}><a href="{{ url('/') }}">Beranda</a></li>
                <li {{ Request::path() === 'produk-promo' ? "class=active" : null }}><a href="/produk-promo">Promo</a></li>
                <li {{ Request::path() === 'web-kategori-produk/2' ? "class=active" : null }}><a href="{{ url('/web-kategori-produk/2') }}">Laptop</a></li>
                <li {{ Request::path() === 'web-kategori-produk/1' ? "class=active" : null }}><a href="{{ url('/web-kategori-produk/1') }}">PC Rakitan</a></li>
                <li {{ Request::path() === 'web-kategori-produk/4' ? "class=active" : null }}><a href="{{ url('/web-kategori-produk/4') }}">Spare Part</a></li>
                <li {{ Request::path() === 'web-kategori-produk/6' ? "class=active" : null }}><a href="{{ url('/web-kategori-produk/6') }}">Asesories</a></li>
                @if(Auth::check() && Auth::user()->role === 'admin')
                    <li {{ Request::path() === '/dashboard' ? "class=active" : null }}><a href="{{ url('/dashboard') }}">Panel Admin</a></li>
                @endif
                @if(Auth::check() && (Auth::user()->role === 'user' || Auth::user()->role === 'admin'))
                    <li {{ Request::path() === 'profile/' . Auth::user()->id ? "class=active" : null }}><a href="{{ url('/profile/' . Auth::user()->id) }}">Profil Saya</a></li>
                @endif
            </ul>
            <!-- /NAV -->
        </div>
        <!-- /responsive-nav -->
    </div>
    <!-- /container -->
</nav>