<!-- HEADER -->
<header>
    <!-- TOP HEADER -->
    <div id="top-header">
        <div class="container">
            <ul class="header-links pull-right">
                @if (Auth::check())
                    <li>
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-email"></i> <span class="test-jquery">{{ Auth::user()->email }} &nbsp; 
                                    <i class="fa fa-toggle-down"></i></span></a>
                            <div class="user-dropdown">
                                <ul>
                                    <li><a href="#"><i class="fa fa-user"></i>Profil Saya</a></li>
                                    <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                @else 
                    <li>
                        <a href="{{ route('login.user') }}">
                            <i class="fa fa-sign-in"></i> <span class="test-jquery">Login</span>
                        </a>	
                    </li>
                    <li>
                        <a href="{{ route('daftar.user.baru') }}">
                            <i class="fa fa-user-plus"></i> <span class="test-jquery">Daftar</span>
                        </a>									
                    </li>
                @endif
            </ul>
        </div>
    </div>
    <!-- /TOP HEADER -->


    <!-- MAIN HEADER -->
    <div id="header">
        <!-- container -->
        <div class="container">
            @if(Auth::check())
                @if((!Auth::user()->status_verifikasi) && (!session()->has('pesan')))
                    <div class="row">
                        <div class="col-sm-12 col-md-offset-3 col-md-6">
                            <div class="alert alert-success">
                                Akun anda belum diverifikasi, silahkan cek email anda untuk verifikasi atau klik 
                                <a href="{{ route('verifikasi.ulang', ['id' => Auth::user()->id]) }}">tautan ini</a> untuk verifikasi ulang.
                            </div>
                        </div>
                    </div>
                @elseif (session()->has('pesan'))
                    <div class="row">
                        <div class="col-sm-12 col-md-offset-3 col-md-6">
                            <div class="alert alert-success">
                                {{ session('pesan') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if(session()->has('error'))
                <div class="row">
                    <div class="col-sm-12 col-md-offset-3 col-md-6">
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div>
                </div>
                @endif
            @endif

            <!-- row -->
            <div class="row">
                <!-- LOGO -->
                <div class="col-md-3">
                    <div class="header-logo">
                        <a href="{{ url('/') }}" class="logo">
                            <img src="{{ asset('web//img/logo-3.png') }}" alt="">
                        </a>
                    </div>
                </div>
                <!-- /LOGO -->

                <!-- SEARCH BAR -->
                <div class="col-md-6">
                    <div class="header-search">
                        {!! Form::open(['route' => ['dapatkan.kategori.produk'], 'method' => 'GET']) !!}
                            <select name="kategori_id" id="kategori_id" class="input-select">
                                <option value="">--- Pilih Kategori ---</option>
                                @foreach ($kategoriProduk as $kategori)
                                    <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                                @endforeach
                            </select>
                            <input class="input" name="cari" placeholder="Cari disini">
                            <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- /SEARCH BAR -->

                <!-- ACCOUNT -->
                <div class="col-md-3 clearfix">
                    <div class="header-ctn">
                        <!-- Cart -->
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Keranjang</span>
                                @if($keranjang && count($keranjang) > 0)
                                    <div class="qty">{{ count($keranjang) }}</div>
                                @endif
                            </a>
                            <div class="cart-dropdown">
                                @if($keranjang && count($keranjang) > 0)
                                    <div class="cart-list">
                                        @foreach($keranjang as $k)
                                        <div class="product-widget">
                                            <div class="product-img">
                                                <img src="{{ asset('web/img/produk/' . $k['produk_gambar']) }}" alt="">
                                            </div>
                                            <div class="product-body">
                                                <h3 class="product-name"><a href="{{ route('spesifik.produk', $k['produk_id']) }}">{{ $k['produk_nama'] }}</a></h3>
                                                <h4 class="product-price"><span class="qty">{{ $k['qty'] }}x</span>Rp {{ formatUang($k['produk_harga']) }},00</h4>
                                            </div>
                                            <a class="delete" href="{{ route('hapus.item.keranjang', $k['produk_id']) }}"><i class="fa fa-close"></i></a>
                                        </div>											
                                        @endforeach
                                    </div>
                                    <div class="cart-summary">
                                        <small>{{ count($keranjang) }} Barang dipilih</small>
                                        <?php
                                            $total_harga = 0;
                                            $total_berat = 0;
                                            
                                            foreach($keranjang as $k) {
                                                $total_harga += ($k['produk_harga'] * $k['qty']);
                                                $total_berat += ($k['produk_berat'] * $k['qty']) / 1000;		
                                            }
                                        ?>
                                        <h6>Total Berat : {{ $total_berat }} Kg</h6>
                                        <h5>SUBTOTAL: Rp {{ formatUang($total_harga) }},00</h5>
                                    </div>
                                    <div class="cart-btns">
                                        <a href="{{ url('/cekout') }}">Cekout  <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                @else 
                                    <div>
                                        <span>Keranjang belanja anda masih kosong</span>
                                    </div>
                                    
                                @endif
                            </div>
                        </div>
                        <!-- /Cart -->

                        <!-- Menu Toogle -->
                        <div class="menu-toggle">
                            <a href="#">
                                <i class="fa fa-bars"></i>
                                <span>Menu</span>
                            </a>
                        </div>
                        <!-- /Menu Toogle -->
                    </div>
                </div>
                <!-- /ACCOUNT -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </div>
    <!-- /MAIN HEADER -->
</header>
<!-- /HEADER -->


