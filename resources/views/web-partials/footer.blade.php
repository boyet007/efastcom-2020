<!-- FOOTER -->
<footer id="footer">
    <!-- top footer -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Tentang Kami</h3>
                        <p>Kami menjual segala hal hal yang berhubungan dengan perangkat teknologi informasi</p>
                        <ul class="footer-links">
                            <li><i class="fa fa-map-marker"></i>Kota Bekasi</li>
                            <li><i class="fa fa-phone"></i>+06221-847-1337</li>
                            <li><i class="fa fa-envelope-o"></i>efastcom@yahoo.com</li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Kategori</h3>
                        <ul class="footer-links">
                            <li><a href="/produk-promo">Promo</a></li>
                            <li><a href="/web-kategori-produk/1">PC Rakitan</a></li>
                            <li><a href="/web-kategori-produk/2">Laptop</a></li>
                            <li><a href="/web-kategori-produk/4">Spare Part PC</a></li>
                            <li><a href="/web-kategori-produk/5">Network</a></li>
                        </ul>
                    </div>
                </div>

                <div class="clearfix visible-xs"></div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Informasi</h3>
                        <ul class="footer-links">
                            <li><a href="#">Tentang Kami</a></li>
                            <li><a href="#">Hubungi Kami</a></li>
                            <li><a href="#">Pemesanan dan Klaim</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-xs-6">
                    <div class="footer">
                        <h3 class="footer-title">Service</h3>
                        <ul class="footer-links">
                            <li><a href="#">Akun Saya</a></li>
                            <li><a href="#">Lihat Keranjang</a></li>
                            <li><a href="#">Bantuan</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /top footer -->

    <!-- bottom footer -->
    <div id="bottom-footer" class="section">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12 text-center">
                    <span class="copyright">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved 
                             by <a href="#" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </span>
                </div>
            </div>
                <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /bottom footer -->
</footer>
<!-- /FOOTER -->