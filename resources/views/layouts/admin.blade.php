<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Dashboard - eFastcom</title>
    <link href="{{ asset('admin/css/styles.css') }}" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
        crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"
        crossorigin="anonymous"></script>
    <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet" />
    @stack('styles')
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-primary">
        <a class="navbar-brand" href="{{ route('index') }}">eFastcom Indonesia</a>
        <ul class="navbar-nav d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"><span>{{ Auth::user()->email }} &nbsp;</span></a>
                <div class="dropdown-menu" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-primary" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Beranda</div>
                        <a class="nav-link" href="{{ route('dashboard') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>

                        <div class="sb-sidenav-menu-heading">Master</div>
                        <a class="nav-link" href="{{ route('satuan.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Satuan
                        </a>
                        <a class="nav-link" href="{{ route('kota.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Kota
                        </a>

                        <a class="nav-link" href="{{ route('kategori-produk.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Kategori Produk
                        </a>
                        <a class="nav-link" href="{{ route('merek.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Merek
                        </a>
                        <a class="nav-link" href="{{ route('user.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            User
                        </a>
                        <a class="nav-link" href="index.html">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Supplier
                        </a>
                        <a class="nav-link" href="{{ route('admin-produk.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Produk
                        </a>
                        <div class="sb-sidenav-menu-heading">Transaksi</div>
                        <a class="nav-link" href="{{ route('order.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                            Penjualan
                        </a>
                      
                        <div class="sb-sidenav-menu-heading">Setting</div>
                        <a class="nav-link" href="charts.html">
                            <div class="sb-nav-link-icon"><i class="fas fa-id-card"></i></div>
                            Profile
                        </a><a class="nav-link" href="tables.html">
                            <div class="sb-nav-link-icon"><i class="fas fa-edit"></i></div>
                            Konten Web
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                @yield('content')
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Philip 2020</div>
                    </div>
                </div>
            </footer>
        </div>
        @include('layouts._modal')
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
        crossorigin="anonymous"></script>
    <script src="{{ asset('admin/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('admin/demo/chart-bar-demo.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin/demo/datatables-demo.js') }}"></script>
    <script src="{{ asset('admin/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('admin/js/moment.js') }}"></script>
    <script src="{{ asset('admin/js/accounting.min.js') }}"></script>
    <script src="{{ asset('admin/js/custom.js') }}"></script>
    @stack('scripts')
</body>

</html>