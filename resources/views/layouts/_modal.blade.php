 <!-- Modal -->
 <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header" id="modal-header">
            <h5 class="modal-title" id="modal-title">Entry</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="modal-body">
        </div>
        <div class="modal-footer" id="modal-footer">
            <button type="button" class="btn btn-warning" id="modal-btn-close" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success" id="modal-btn-save" >Save changes</button>
        </div>
        </div>
    </div>
</div>