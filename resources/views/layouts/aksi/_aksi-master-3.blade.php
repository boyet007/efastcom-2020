<!-- aksi master produk -->

<a href="{{ $url_show }}" class="btn-tampil-modal" ukuran="besar" tipe="lihat" judul="Detail: {{ $model->nama }}">
    <i class="fas fa-eye text-primary"></i></a>

<a href="{{ $url_edit }}" class="btn-tampil-modal" tipe="edit" data-kode="{{ $model->id }}" 
        ukuran="besar" judul="Edit: {{ $model->nama }}">
    <i class="fas fa-pencil-alt text-warning"></i></a>

<a href="{{ $url_destroy }}" judul='{{ $model->nama }}' 
    class="btn-hapus"><i class="fas fa-trash text-danger"></i></a>