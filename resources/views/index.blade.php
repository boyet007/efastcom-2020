@extends('layouts.web')

@section('title', $title)

@section('content')
		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					@foreach($indexKategori as $i)
						<div class="col-md-4 col-xs-6">
							<div class="shop">
								<div class="shop-img">
									<img src="{{ url('/web/img/' . $i->gambar) }}" alt="">
								</div>
								<div class="shop-body">
									<h3>Koleksi<br>{{ $i->nama }}</h3>
									<a href="{{ url('/web-kategori-produk/' . $i->id) }}" class="cta-btn">Belanja <i class="fa fa-arrow-circle-right"></i></a>
								</div>
							</div>
						</div>
					@endforeach
					<!-- /shop -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Produk Baru</h3>
						</div>
					</div>
					<!-- /section title -->
					@if($produkBaru->count() > 0)
						<!-- Products tab & slick -->
						<div class="col-md-12">
							<div class="row">
								<div class="products-tabs">
									<!-- tab -->
										<div class="products-slick" data-nav="#slick-nav-1">

											@foreach($produkBaru as $p)
												<!-- product -->
												<div class="product">
													<div class="product-img">
														<img src="{{ url('/web/img/produk/' . $p->gambar) }}" alt="">
														<div class="product-label">
															@if($p->diskon > 0)
																<span class="sale">{{ $p->diskon }} %</span>
															@endif
															<span class="new">BARU</span>
														</div>
													</div>
													<div class="product-body">
														<p class="product-kategory">{{ $p->kategori_produk->nama }}</p>
														<h3 class="product-name"><a href="{{ url('/produk/' . $p->id) }}">{{ $p->nama }}</a></h3>
														@if(isset($p->diskon))
															<h4 class="product-price">Rp {{ format_uang($p->hargaSetelahDiskon()) }},00
																<del class="product-old-price">Rp {{ format_uang($p->harga) }},00</del>
															</h4>
														@else 
															<h4 class="product-price">Rp {{ format_uang($p->harga) }},00</h4>
														@endif
													</div>
													<div class="add-to-cart">
														{!! Form::open(['route' => 'tambah.keranjang', 'method' => 'POST', 
														'novalidate' => 'novalidate']) !!}
															{!! Form::hidden('qty', '1') !!}
															{!! Form::hidden('produk_id', $p->id) !!}
															{!! Form::submit('Tambah Ke Keranjang', ['class' => 'add-to-cart-btn']) !!}
														{!! Form::close() !!}
													</div>

												</div>
												<!-- /product -->
											@endforeach

										
										</div>
										<div id="slick-nav-1" class="products-slick-nav"></div>
									</div>
									<!-- /tab -->
								</div>
							</div>
						</div>
						<!-- Products tab & slick -->
					@else
						<div class="col-md-12">
							<p>Maaf saat ini belum ada produk baru</p>
						</div>
					@endif
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- HOT DEAL SECTION -->
		<div id="hot-deal" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="hot-deal">
							<h2 class="text-uppercase">Promo besar minggu ini!!</h2>
							<?php 
								$diskonTerbesar = App\Produk::max('diskon');
							?>
							<p>Koleksi dengan diskon sampai {{ $diskonTerbesar }}%</p>
							<a class="primary-btn cta-btn" href="{{ url('/produk') }}">Belanja Sekarang</a>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /HOT DEAL SECTION -->
@stop

