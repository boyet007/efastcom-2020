<!DOCTYPE html>
<html lang="en" style="margin: 0;">
    <head>
        <!-- NAME: 1 COLUMN -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>
    </head>
<body style="margin: 36px 65px;font-family: cursive;">
    <div class="kop-image" style="margin: 0;">
        <img src="/web/img/logo-3.png" alt="" style="margin: 0;">
    </div>
    <div class="kop-header" style="margin: 0;">
        <span style="margin: 0;display: block;margin-bottom: 5px;">Kepada Yth</span>
        <span style="margin: 0;display: block;"><strong style="margin: 0;">{{ $user->nama_lengkap }}</strong></span>

        <span style="margin: 0;display: block;">{{ $user->alamat }}</span>
        <span style="margin: 0;display: block;">{{ $order->kabupaten }}</span>
        <span style="margin: 0;display: block;">{{ $order->provinsi }}</span>
        <span style="margin: 0;display: block;margin-bottom: 15px;">{{ $order->kode_pos }}</span>

    </div>
    <div class="main" style="margin: 0;">
        <p style="margin: 0;">
            Terima kasih telah berbelanja diwebsite online kami eFastcom Indonesia, berikut adalah besar tagihan
            yang belum anda bayarkan untuk melanjutkan proses selanjutnya.
        </p>
        <table style="margin: 30px 35px;width: 75%;font-size: smaller;border: 2px solid #a5b4cc;border-collapse: collapse;min-height: 250px;">
            <thead style="margin: 0;">
                <tr style="margin: 0;">
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">NO</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">NAMA BARANG</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">QTY</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">HARGA</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">DISC</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #52555a;">HARGA NETT</th>
                    <th style="margin: 0;border-right: 1px solid #a5b4cc;border-bottom: 1px solid #a5b4cc;">TOTAL</th>
                </tr>
            </thead>
            <tbody style="margin: 0;">
                <?php 
                    $no = 1;
                    $subTotal = 0;
                    $ongkir = $order->ongkir;
                    $grandTotal = 0;
                ?> 
                @foreach($keranjang as $k)
                    <?php 
                        $produkNama = $k['produk_nama'];
                        $produkDiskon = $k['produk_diskon']; 
                        $qty = (int)$k['qty'];
                        $produkSatuan = $k['produk_satuan'];
                        $produkHarga = $k['produk_harga'];
                        $produkDiskon ? $produkDiskon : 0;
                        $hargaSetelahDiskon = $produkHarga - $produkHarga * $produkDiskon / 100;                        
                        $total = $hargaSetelahDiskon * $qty;
                        $subTotal += $total;
                    ?>  

                    <tr style="height: 10px;margin: 0;">
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;">{{ str_pad($no, 2, '0', STR_PAD_LEFT) }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;padding-left: 5px;">{{ $produkNama }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;">{{ $qty }} {{ $produkSatuan }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;">{{ formatUang($produkHarga) }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;">{{ $k['produk_diskon'] ? $k['produk_diskon'] . ' %' : '-' }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;">{{ formatUang($hargaSetelahDiskon) }}</td>
                        <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;">{{ formatUang($total) }}</td>
                    </tr>
                    <?php $no++;  ?>
                @endforeach
                <tr style="margin: 0;border-bottom: 1px solid #a5b4cc;">
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;padding-left: 5px;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: center;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;"></td>
                    <td style="margin: 0;border-right: 1px solid #a5b4cc;text-align: right;padding-right: 5px;"></td>
                </tr>
            </tbody>
            <tfoot style="margin: 0;">
                <?php $grandTotal = $subTotal + $ongkir; ?>
                <tr style="margin: 0;">
                    <td colspan="5" style="margin: 0;"><strong style="margin: 0;">Terbilang : {{ terbilang($grandTotal) }}</strong></td>
                    <td style="margin: 0;border-left: 1px solid #a5b4cc;border-right: 1px solid #a5b4cc;padding-left: 5px;font-weight: bold;border-bottom: 1px solid #a5b4cc;">Sub Total</td>
                    <td style="margin: 0;padding-left: 5px;text-align: right;padding-right: 5px;font-weight: bold;border-bottom: 1px solid #a5b4cc;">Rp {{ formatUang($subTotal) }}</td>
                </tr>
                <tr style="margin: 0;">
                    <td colspan="5" style="margin: 0;"></td>
                    <td style="margin: 0;border-left: 1px solid #a5b4cc;border-right: 1px solid #a5b4cc;padding-left: 5px;border-bottom: 1px solid #a5b4cc;">Ongkir</td>
                    <td style="margin: 0;padding-left: 5px;text-align: right;padding-right: 5px;border-bottom: 1px solid #a5b4cc;">Rp {{ formatUang($ongkir) }}</td>
                </tr>
                <tr style="margin: 0;">
                    <td colspan="5" style="margin: 0;">Transfer BCA No. 829 2931 atas Nama PT. eFastcom Indoneisa</td>
                    <td style="margin: 0;border-left: 1px solid #a5b4cc;border-right: 1px solid #a5b4cc;padding-left: 5px;font-weight: bold;">Grand Total</td>
                    <td style="margin: 0;padding-left: 5px;text-align: right;padding-right: 5px;font-weight: bold;">Rp {{ formatUang($grandTotal) }}</td>
                </tr>
            </tfoot>
        </table>
        <div class="auto-message" style="margin: 0;">
            Ini adalah email otomatis yang digenerate oleh sistem kami, mohon tidak mereply email ini.
        </div>
    </div>
</body>
</html>