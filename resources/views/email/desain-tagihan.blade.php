<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        *{ margin:0; }

        body{
            font-family: cursive;
            margin:36px 65px;
        }

        .kop-header > span {
            display:block;
        }

        .kop-header > span:first-child { 
            margin-bottom: 5px;
        }

        .kop-header > span:last-child { 
            margin-bottom: 15px;
        }

        table{
            margin: 30px 35px;
            width: 75%;
            font-size: smaller;
            border: 2px solid #a5b4cc;
            border-collapse: collapse;
            min-height: 250px;
        }

        table > thead > tr:first-child > th {
            border-bottom: 1px solid #a5b4cc;
        }

        table > thead > tr > th,
        table > tbody > tr > td {
            border-right: 1px solid #a5b4cc;
        }

        table > tbody > tr > td:first-child,
        table > tbody > tr > td:nth-child(3),
        table > tbody > tr > td:nth-child(5)
         {
            text-align: center;
         }

         table > tbody > tr > td:nth-child(2) {
             padding-left:5px;
         }

         table > tbody > tr > td:nth-child(4), 
         table > tbody > tr > td:nth-child(6), 
         table > tbody > tr > td:nth-child(7) {
            text-align: right;
            padding-right:5px;
         }

         table > tbody > tr:last-child
         {
            border-bottom: 1px solid #a5b4cc;
         }

         table > tfoot > tr > td:nth-child(2) {
             border-left: 1px solid #a5b4cc;
             border-right: 1px solid #a5b4cc;
         }

         table > tfoot > tr > td:nth-child(2), 
         table > tfoot > tr > td:nth-child(3) {
             padding-left:5px;
         }

         table > tfoot > tr:first-child > td:nth-child(2),
         table > tfoot > tr:first-child > td:nth-child(3),
         table > tfoot > tr:nth-child(3) > td:nth-child(2),
         table > tfoot > tr:nth-child(3) > td:nth-child(3) {
             font-weight: bold;
         }

         table > tfoot > tr > td:nth-child(3) {
             text-align: right;
             padding-right:5px;
         }

         table > tfoot > tr:first-child > td:nth-child(2),
         table > tfoot > tr:nth-child(2) > td:nth-child(2),
         table > tfoot > tr:first-child > td:nth-child(3),
         table > tfoot > tr:nth-child(2) > td:nth-child(3) {
             border-bottom: 1px solid #a5b4cc;
         }


    </style>
</head>
<body>
    <div class="kop-image">
        <img src="/web/img/logo-3.png" alt="">
    </div>
    <div class="kop-header">
        <span>Kepada Yth</span>
        <span><strong>Wynne Kurniawan</strong></span>

        <span>Jl. Keadilan No. 5</span>
        <span>Jakarta Barat</span>
        <span>DKI Jakarta</span>
    </div>
    <div class="main">
        <p>
            Terima kasih telah berbelanja diwebsite online kami eFastcom Indonesia, berikut adalah besar tagihan
            yang belum anda bayarkan untuk melanjutkan proses selanjutnya.
        </p>
        <table>
            <thead>
                <tr>
                    <th>NO</th>
                    <th>NAMA BARANG</th>
                    <th>QTY</th>
                    <th>HARGA</th>
                    <th>DISC</th>
                    <th>HARGA NETT</th>
                    <th>SUBTOTAL</th>
                </tr>
            </thead>
            <tbody>
                <tr style="height: 10px">
                    <td>01</td>
                    <td>Memori 4 gb ddr 3</td>
                    <td>1 PCS</td>
                    <td>250.000</td>
                    <td>5%</td>
                    <td>237.500</td>
                    <td>237.500</td>
                </tr>
                <tr style="height: 10px">
                    <td>02</td>
                    <td>Speaker Altec Lansing</td>
                    <td>1 PCS</td>
                    <td>980.000</td>
                    <td>-</td>
                    <td>980.000</td>
                    <td>980.000</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5"><strong>Terbilang : Satu Juta Tujuh Ribu Lima Ratus Rupiah</strong></td>
                    <td>Sub Total</td>
                    <td>Rp 1.007.000,00</td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Ongkir</td>
                    <td>Rp 56.000,00</td>
                </tr>
                <tr>
                    <td colspan="5">Transfer BCA No. 829 2931 atas Nama PT. eFastcom Indoneisa</td>
                    <td>Grand Total</td>
                    <td>Rp 1.061.000,00</td>
                </tr>
            </tfoot>
        </table>
        <div class="auto-message">
            Ini adalah email otomatis yang digenerate oleh sistem kami, mohon tidak mereply email ini.
        </div>
    </div>
</body>
</html>