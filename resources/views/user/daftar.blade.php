@extends('layouts.web')

@section('content')
    <div id="breadcrumb" class="section" style="margin-bottom:0px;">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="breadcrumb-header">Pendaftaran User Baru</h3>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
        
    <div class="section daftar">
        <!-- container -->
        <div class="container">
            <!-- row -->
        
            @if (session('error'))
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div> 
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="card login">
                        <div class="card-body">
                        {!! Form::open(['route' => 'post.daftar.baru', 'method' => 'POST', 
                            'novalidate' => 'novalidate']) !!}
                            <div class="input-group mb-2">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Masukkan Email']); !!}
                            </div>
                            @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Masukkan Password']); !!}
                            </div>
                            @if($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Masukkan Password Ulang']); !!}
                            </div>
                            @if($errors->has('password_confirmation'))
                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                            @endif

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                {!! Form::text('nama_lengkap', null, ['class' => 'form-control', 'placeholder' => 'Masukkan Nama Lengkap']); !!}
                            </div>
                            @if($errors->has('nama_lengkap'))
                                <span class="help-block">{{ $errors->first('nama_lengkap') }}</span>
                            @endif

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hotel"></i></span>
                                {!! Form::textarea('alamat', null, ['class' => 'form-control', 'placeholder' => 'Masukkan Alamat', 'rows' => '3']); !!}
                            </div>
                            @if($errors->has('alamat'))
                                <span class="help-block">{{ $errors->first('alamat') }}</span>
                            @endif

                            <div class="form-group custom-input">
                                <span class="input-group-addon span-kota"><i class="fa fa-home"></i></span>
                                <select name="kabupaten_id" id="kabupaten_id" class="form-control">
                                    <option value="">--- Pilih Kabupaten / Kota ---</option>
                                    @foreach ($daftarKabupaten as $kabupaten  )
                                        <option value="{{ $kabupaten['city_id'] }}">{{ $kabupaten['city_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('kabupaten_id'))
                                <span class="help-block">{{ $errors->first('kabupaten_id') }}</span>
                            @endif
                            
                            <div class="form-group custom-input">
                                <span class="input-group-addon span-kota"><i class="fa fa-map"></i></span>
                                <select name="provinsi_id" id="provinsi_id" class="form-control">
                                    <option value="">--- Pilih Provinsi ---</option>
                                    @foreach ($daftarProvinsi as $provinsi)
                                        <option value="{{ $provinsi['province_id'] }}">{{ $provinsi['province'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($errors->has('provinsi_id'))
                                <span class="help-block">{{ $errors->first('provinsi_id') }}</span>
                            @endif

                            <div class="input-group" style="width:59%;">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                {!! Form::text('kode_pos', null, ['id' => 'kode_pos', 'class' => 'form-control', 'placeholder' => 'Kode Pos', 'disabled' => 'disabled']) !!}
                            </div>

                            <div class="input-group" style="width:59%;">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                {!! Form::text('no_telpon', null, ['class' => 'form-control', 'placeholder' => 'Masukkan No Telpon']) !!}
                            </div>
                            @if($errors->has('no_telpon'))
                                <span class="help-block">{{ $errors->first('no_telpon') }}</span>
                            @endif

                            <div class="form-group">
                                {!! Form::submit('Daftar', ['class' => 'btn pull-right login_btn']) !!}
					        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
@stop
