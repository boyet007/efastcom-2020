@extends('layouts.web')

@section('content')
    <div id="breadcrumb" class="section" style="margin-bottom:0px;">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="breadcrumb-header">Area Login</h2>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
        
    <div class="section login">
        <!-- container -->
        <div class="container">
            <!-- row -->
            @if (session('pesan_verifikasi'))
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="alert alert-success">
                        {{ session('pesan_verifikasi') }}
                    </div>
                </div>
            </div>
            @endif

            @if (session('error'))
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    </div> 
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                            {!! Form::open(['route' => 'post.login.user']) !!}
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    {!! Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Masukkan Email']); !!}  
                                </div>
                                @if($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    {!! Form::password('password', ['id' => 'password', 'class' => 'form-control', 'placeholder' => 'Masukkan Password']); !!}
                                </div>
                                @if($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif

                                <div class="form-group">
                                    {!! Form::submit('Login', ['id' => 'btn-login', 'class' => 'btn pull-right login_btn']) !!}
                                </div>
                            {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
@stop