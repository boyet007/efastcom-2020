<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'kota';
    protected $fillable = ['kode', 'nama'];
    public $timestamps = false;

    public function user() {
        return $this->hasMany(User::class);
    }

    public function supplier() {
        return $this->hasMany(Supplier::class);
    }
}
