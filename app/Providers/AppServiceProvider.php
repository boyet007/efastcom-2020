<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\KategoriProduk;
use Illuminate\Http\Request;
use Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['web-partials.header'], function($view) {
            $kategoriProduk = KategoriProduk::all();
            $keranjang = null;
            if(Auth::User() && request()->cookie('keranjang')) {
                $keranjang = json_decode(request()->cookie('keranjang'), true);
            } 

            $view->with('kategoriProduk', $kategoriProduk)
                ->with('keranjang', $keranjang);
        });
    }
}
