<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = 'kategori_produk';
    protected $fillable = ['nama', 'gambar'];
    public $timestamps = false;

    public function produk() {
        return $this->hasMany(Produk::class, 'kategori_id', 'id');
        
    }
}
