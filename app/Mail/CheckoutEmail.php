<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CheckoutEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $keranjang;
    public $order;
    public function __construct($parUser, $parOrder, $parKeranjang)
    {   
        $this->user = $parUser;
        $this->order = $parOrder;
        $this->keranjang = $parKeranjang;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $order = $this->order;
        $keranjang = $this->keranjang;
        return $this->view('email.tagihan-email')
        ->from([
            'address' => 'admin@efastcom-indonesia.com',
            'name' => 'Efastcom Indonesia'
        ])
        ->subject('Detail pembelian barang di toko online eFastcom')
        ->with([
            'user' => $user,
            'order' => $order,
            'keranjang' => $keranjang 
        ]);
    }
}
