<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['nama', 'kategori_id', 'supplier_id', 'harga', 'harga_beli', 'qty', 'merek_id',
      'diskon', 'tanggal_beli', 'satuan_id', 'berat', 'ket', 'gambar'];
    public $timestamps = false;

    public function kategori_produk() {
        return $this->belongsTo(KategoriProduk::class, 'kategori_id', 'id');
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'id');
        //return $this->belongsToMany(Supplier::class)->withPivot(['tanggal_beli', 'qty', 'harga_beli'])->withTimestamps();
    } 

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'satuan_id', 'id');
    }

    public function merek() {
        return $this->belongsTo(Merek::class, 'merek_id', 'id');
    }

    public function order() {
        return $this->belongsToMany(Order::class);
    }

    public function hargaSetelahDiskon() {
        $diskon = $this->diskon;
        $harga = $this->harga;
        $harga_baru = $harga - ($harga * $diskon / 100);
        return $harga_baru;
    }
}
