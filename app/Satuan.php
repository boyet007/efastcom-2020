<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    protected $table = 'satuan';
    protected $fillable = ['kode', 'nama'];
    public $timestamps = false;

    public function produk() {
        return $this->hasMany(Produk::class, 'satuan_id', 'id');
    }
}
