<?php

namespace App\Http\Middleware;

use Closure;

class CheckVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$is_verify)
    {
        if(in_array($request->user()->status_verifikasi, $is_verify)) {
            return $next($request);
        }
        return redirect('/')->with('pesan', 'Maaf akun anda belum terverifikasi silahkan refresh halaman ini untuk proses verifikasi');
    }
}
