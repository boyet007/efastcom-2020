<?php

namespace App\Http\Controllers;
use App\KategoriProduk;
use DataTables;
use Illuminate\Http\Request;
use Image;
use File;


class KategoriProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sub_title = 'Data Kategori Produk'; 
        $form_title = 'Tambah Data Kategori Produk';
        return view('admin.master.kategori-produk.index', compact(['sub_title', 'form_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new KategoriProduk();
        return view('admin.master.kategori-produk.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new KategoriProduk();
        cekNamaFile($model, $request->file);

        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:kategori_produk',
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
       
        $namaGambar = $request->file->getClientOriginalName();
      
        //upload file
        $lebar = config('info.GAMBAR_KATEGORI_PRODUK_LEBAR');
        $tinggi = config('info.GAMBAR_KATEGORI_PRODUK_TINGGI');
        $gambar = Image::make($request->file('file'))->resize($lebar, $tinggi)->save(public_path('web/img') . '/' . $namaGambar);
        
        //tambahkan request gambar 
        $request->request->add(['gambar' => $namaGambar]);
        
        $model = KategoriProduk::create($request->all());
        
        $gambar->destroy();

        return $model;      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = KategoriProduk::findOrFail($id);
        return view('admin.master.kategori-produk.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = KategoriProduk::findOrFail($id);
        return view('admin.master.kategori-produk.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file) {
            $model = new KategoriProduk();
            cekNamaFile($model, $request->file);
            
            $this->validate($request, [
                'nama' => 'required|string|max:50|unique:kategori_produk,id,' . $id,
                'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);
    
            //remove old file from storage disk
            if(file_exists(public_path('web/img/') . $request->nama_file_lama)) {
                unlink(public_path('web/img/') . $request->nama_file_lama);
            }
            
            $namaGambar = $request->file->getClientOriginalName();
          
            //upload file
            $lebar = config('info.GAMBAR_KATEGORI_PRODUK_LEBAR');
            $tinggi = config('info.GAMBAR_KATEGORI_PRODUK_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebar, $tinggi)->save(public_path('web/img') . '/' . $namaGambar);
    
            //tambahkan request gambar 
            $request->request->add(['gambar' => $namaGambar]);
    
            $model = KategoriProduk::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'nama' => 'required|string|max:50|unique:kategori_produk,id,' . $id
            ]);
            $model = KategoriProduk::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = KategoriProduk::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if(file_exists(public_path('web/img/') . $gambar)) {
            unlink(public_path('web/img/') . $gambar);
        }
    }

    public function dataTable() 
    {
        $model = KategoriProduk::query();
          
        return Datatables::of($model)  
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master', [
                     'model' => $model,
                     'url_show' => route('kategori-produk.show', $model->id),
                     'url_edit' => route('kategori-produk.edit', $model->id),
                     'url_destroy' => route('kategori-produk.destroy', $model->id)
                 ]);   
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
