<?php

namespace App\Http\Controllers;
use App\Kota;
use DataTables;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_title = 'Data Kota'; 
        $form_title = 'Tambah Data Kota';
        return view('admin.master.kota.index', compact(['sub_title', 'form_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Kota();
        return view('admin.master.kota.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required|string|min:3|max:3|unique:kota',
            'nama' => 'required|string|max:50',
        ]);
        $model = Kota::create($request->all());
        return $model;    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Kota::findOrFail($id);
        return view('admin.master.kota.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Kota::findOrFail($id);
        return view('admin.master.kota.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'kode' => 'required|string|max:3|unique:kota,id,' . $id,
            'nama' => 'required|string|max:50',
        ]);

        $model = Kota::findOrFail($id);
        $model->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Kota::findOrFail($id);
        $model->delete();
    }

    public function dataTable() 
    {
        $model = Kota::query();
          
        return Datatables::of($model)  
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master', [
                     'model' => $model,
                     'url_show' => route('kota.show', $model->id),
                     'url_edit' => route('kota.edit', $model->id),
                     'url_destroy' => route('kota.destroy', $model->id)
                 ]);   
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
