<?php

namespace App\Http\Controllers;
use App\Merek;
use DataTables;
use Illuminate\Http\Request;
use Image;
use File;
use Illuminate\Support\Facades\Storage;

class MerekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_title = 'Data Merek'; 
        $form_title = 'Tambah Data Merek';
        return view('admin.master.merek.index', compact(['sub_title', 'form_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Merek();
        return view('admin.master.merek.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Merek();
        cekNamaFile($model, $request->file);

        $this->validate($request, [
            'nama' => 'required|string|max:50|unique:merek',
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $namaGambar = $request->file->getClientOriginalName();

          //upload file
          $lebar = config('info.GAMBAR_KATEGORI_MEREK_LEBAR');
          $gambar = Image::make($request->file('file'))->resize($lebar, null, function($constraint) use ($namaGambar) {
               $constraint->aspectRatio(public_path('web/img') . '/' . $namaGambar);
          });
          
          $gambar->save(public_path('web/img') . '/' . $namaGambar);
          
          //tambahkan request gambar 
          $request->request->add(['gambar' => $namaGambar]);
          
          $model = Merek::create($request->all());
          $gambar->destroy();
        return $model;      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Merek::findOrFail($id);
        return view('admin.master.merek.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $model = Merek::findOrFail($id);
       
        return view('admin.master.merek.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->file) {
            $model = new Merek();
            cekNamaFile($model, $request->file);

            $this->validate($request, [
                'nama' => 'required|string|max:50|unique:merek,id,' . $id,
                'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            //remove old file from storage disk
            if(file_exists(public_path('web/img/') . $request->nama_file_lama)) {
                unlink(public_path('web/img/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            //upload file
            $lebar = config('info.GAMBAR_KATEGORI_MEREK_LEBAR');
            $gambar = Image::make($request->file('file'))->resize($lebar, null, function($constraint) use ($namaGambar) {
                $constraint->aspectRatio(public_path('web/img') . '/' . $namaGambar);
            });
            $gambar->save(public_path('web/img') . '/' . $namaGambar);
            //tambahkan request gambar 
            $request->request->add(['gambar' => $namaGambar]);
            $model = Merek::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'nama' => 'required|string|max:50|unique:merek,id,' . $id
            ]);
            $model = Merek::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Merek::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if(file_exists(public_path('web/img/') . $gambar)) {
            unlink(public_path('web/img/') . $gambar);
        }
    }

    public function dataTable() 
    {
        $model = Merek::query();
          
        return Datatables::of($model)  
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master', [
                     'model' => $model,
                     'url_show' => route('merek.show', $model->id),
                     'url_edit' => route('merek.edit', $model->id),
                     'url_destroy' => route('merek.destroy', $model->id)
                 ]);   
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
