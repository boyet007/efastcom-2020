<?php

namespace App\Http\Controllers;
use DataTables;
use App\Produk;
use App\KategoriProduk;
use App\Satuan;
use App\Merek;
use App\Supplier;
use Illuminate\Http\Request;
use Image;
use File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_title = 'Data Produk'; 
        $form_title = 'Tambah Data Produk';
        return view('admin.master.produk.index', compact(['sub_title', 'form_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Produk();
        
        $listKategoriProduk = ['' => '=== Pilih Kategori Produk ==='] + KategoriProduk::pluck('nama', 'id')->all();
        $listMerek = ['' => '=== Pilih Merek ==='] + Merek::pluck('nama', 'id')->all();
        $listSatuan = ['' => '=== Pilih Satuan ==='] + Satuan::pluck('kode', 'id')->all();
        $listSupplier = ['' => '=== Pilih Supplier ==='] + Supplier::pluck('nama', 'id')->all();

        return view('admin.master.produk.form', compact(['model', 'listKategoriProduk',  'listMerek', 'listSatuan', 'listSupplier']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Produk();
        cekNamaFile($model, $request->file);

        $request->merge(['harga' => $request->hidden_harga]);
        $request->merge(['harga_beli' => $request->hidden_harga_beli]);

        $this->validate($request, [
            'user_id' => 'required',
            'kategori_id' => 'required',
            'nama' => 'required|string|max:50|unique:produk',
            'qty' => 'required|min:0|numeric',
            'satuan_id' => 'required',
            'harga' => 'required|min:0|numeric',   
            'diskon' => 'numeric|min:0|max:100|nullable',
            'harga_beli' => 'required|min:0|numeric',
            'tanggal_beli' => 'required',
            'merek_id'=> 'required',
            'supplier_id' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $namaGambar = $request->file->getClientOriginalName();
        $lebar = config('info.GAMBAR_PRODUK_LEBAR');
        $tinggi = config('info.GAMBAR_PRODUK_TINGGI');
        $gambar = Image::make($request->file('file'))->resize($lebar, $tinggi)->save(public_path('web/img/produk') . '/' . $namaGambar);

        if ((int)$request->diskon === 0) {
            $request->merge(['diskon' => null]);
        }
        $request->request->add(['gambar' => $namaGambar]);

        $model = Produk::create($request->all());
        $gambar->destroy();
        return $model;      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Produk::findOrFail($id);
        return view('admin.master.produk.tampil', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Produk::findOrFail($id);

        $listKategoriProduk = ['' => '=== Pilih Kategori Produk ==='] + KategoriProduk::pluck('nama', 'id')->all();
        $listMerek = ['' => '=== Pilih Merek ==='] + Merek::pluck('nama', 'id')->all();
        $listSatuan = ['' => '=== Pilih Satuan ==='] + Satuan::pluck('kode', 'id')->all();
        $listSupplier = ['' => '=== Pilih Supplier ==='] + Supplier::pluck('nama', 'id')->all();

        return view('admin.master.produk.form', compact(['model', 'listKategoriProduk',  'listMerek', 'listSatuan', 'listSupplier']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['harga' => $request->hidden_harga]);
        $request->merge(['harga_beli' => $request->hidden_harga_beli]);

        if ($request->file) {
            $model = new Produk();
            cekNamaFile($model, $request->file);

        

            $this->validate($request, [
                'user_id' => 'required',
                'kategori_id' => 'required',
                'nama' => 'required|string|max:50|unique:produk,id,' . $id,
                'qty' => 'required|min:0|numeric',
                'satuan_id' => 'required',
                'harga' => 'required|min:0|numeric',   
                'diskon' => 'numeric|min:0|max:100|nullable',
                'harga_beli' => 'required|min:0|numeric',
                'tanggal_beli' => 'required',
                'merek_id'=> 'required',
                'supplier_id' => 'required',
                'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]);

            //remove old file from storage disk
            if(file_exists(public_path('web/img/produk/') . $request->nama_file_lama)) {
                unlink(public_path('web/img/produk/') . $request->nama_file_lama);
            }

            $namaGambar = $request->file->getClientOriginalName();
            //upload file
            $lebar = config('info.GAMBAR_PRODUK_LEBAR');
            $tinggi = config('info.GAMBAR_PRODUK_TINGGI');
            $gambar = Image::make($request->file('file'))->resize($lebar, $tinggi)->save(public_path('web/img/produk') . '/' . $namaGambar);
            $gambar->save(public_path('web/img/produk') . '/' . $namaGambar);
            //tambahkan request gambar 
            $request->request->add(['gambar' => $namaGambar]);
            $model = Produk::findOrFail($id);
            $model->update($request->all());
            $gambar->destroy();
        } else {
            $namaGambar = $request->nama_file_lama;
            $this->validate($request, [
                'user_id' => 'required',
                'kategori_id' => 'required',
                'nama' => 'required|string|max:50|unique:produk,id,' . $id,
                'qty' => 'required|min:0|numeric',
                'satuan_id' => 'required',
                'harga' => 'required|min:0|numeric',   
                'diskon' => 'numeric|min:0|max:100|nullable',
                'harga_beli' => 'required|min:0|numeric',
                'tanggal_beli' => 'required',
                'merek_id'=> 'required',
                'supplier_id' => 'required',
            ]);
            $model = Produk::findOrFail($id);
            $request->request->add(['gambar' => $namaGambar]);
            $model->update($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Produk::findOrFail($id);
        $gambar = $model->gambar;
        $model->delete();

        if(file_exists(public_path('web/img/produk/') . $gambar)) {
            unlink(public_path('web/img/produk/') . $gambar);
        }
    }

    public function dataTable() 
    {
        $model = Produk::with('kategori_produk', 'supplier', 'satuan', 'merek')->select('produk.*');
          
        return Datatables::of($model)  
        ->addColumn('kategori', function($model) {
            return $model->kategori_produk->nama;
        })
        ->addColumn('satuan', function($model) {
            return $model->satuan->nama;
        })
        ->addColumn('supplier', function($model) {
            return $model->supplier->nama;
        })
        ->addColumn('merek', function($model) {
            return $model->merek->nama;
        })
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master-3', [
                     'model' => $model,
                     'url_show' => route('admin-produk.show', $model->id),
                     'url_edit' => route('admin-produk.edit', $model->id),
                     'url_destroy' => route('admin-produk.destroy', $model->id)
                 ]);   
            })
        ->rawColumns(['aksi'])
        ->make(true);
    }

    public function cariProdukBerdasarkanKategoriId(Request $request) {
        $id = $request->kategoriId;
        $produk = KategoriProduk::find($id)->produk;
        return $produk;
    }

    public function cariMerekLogo(Request $request) {
        $id = $request->merekId;
        $merekLogo = Merek::find($id)->gambar;
        return $merekLogo;
    } 

    public function cariDataSupplier(Request $request) {
        $id = $request->supplierId;
        $dataSupplier = Supplier::with('kota')->find($id);
        return $dataSupplier;
    }

    public function cariNamaSatuan(Request $request) {
        $id = $request->satuanId;
        $namaSatuan = Satuan::find($id)->nama;
        return $namaSatuan;
    }

    public function cariDataProduk(Request $request) {
        $id = $request->produkId;
        return Produk::find($id);
    }

    public function hitungHargaDiskon(Request $request) {
        $hargaBeli = $request->harga;
        $diskon = $request->diskon;
        return $hargaBeli - ( $hargaBeli * $diskon / 100 );
    }

    public function cariGambarProduk(Request $request) {
        $id = $request->id;
        return Produk::find($id)->gambar;
    }
}
