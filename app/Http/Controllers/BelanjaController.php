<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Produk;
use App\Order;
use Auth;
use Cookie;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use Validator;
use Carbon\Carbon;
use Mail;
use App\Mail\CheckoutEmail;;

class BelanjaController extends Controller
{
    public function tambahKeranjang(Request $request) {
        $this->validate($request, [
            'produk_id' => 'required|exists:produk,id',
            'qty' => 'required|numeric|min:0',
        ]);

        $produkId = $request->produk_id;
        $qty = $request->qty;
        $keranjang = json_decode($request->cookie('keranjang'), true); 

        //jika user tidak authenticated
        if (!Auth::check()) {
            session()->now('error', 'Anda harus login untuk belanja');
        }

        $produk = Produk::find($produkId);
        $produkQty= $produk->qty;

        //cek stok
        if ($produkQty < $qty) {
            return redirect()->back()->with('error', 'Kuantiti yang anda pesan lebih besar daripada stok kami');
        }

        //jika produk sudah ada didalam keranjang belanja, maka tambahkan saja quantitynya
        if ($keranjang && array_key_exists($produkId, $keranjang)) {
            (int)$keranjang[$produkId]['qty'] +=  $qty;
        } else {
            //bikin produk baru
            $keranjang[$produkId] = [
                'qty' => $qty,
                'produk_id' => $produkId,
                'produk_nama' => $produk->nama,
                'produk_merek' => $produk->merek->nama,
                'produk_harga' => $produk->harga,
                'produk_satuan' => $produk->satuan->nama,
                'produk_diskon' => $produk->diskon,
                'produk_berat' => $produk->berat,
                'produk_gambar' => $produk->gambar,
            ];
        }
        $cookie = cookie('keranjang', json_encode($keranjang), 2880);

        //kurangi stok dari produk
        $produk->qty = $produkQty - $qty;
        $produk->save();
                
        return redirect()->back()->with('pesan', 'Produk ' . $produk->nama . ' telah ditambahkan ke dalam keranjang')->cookie($cookie);
    }

    public function hapusItemKeranjang($id) {
        $keranjang = json_decode(request()->cookie('keranjang'), true); 
        
        $produkDeleted = array_filter($keranjang, function ($k) use ($id) {
            return $k['produk_id'] === $id;
        });

        $qty = $produkDeleted[$id]['qty'];

        array_pull($keranjang, $id);
        $cookie = cookie('keranjang', json_encode($keranjang), 2880);

        //kembalikan quantity produk ke database
        $produk = Produk::find($id);
        $produkQty= $produk->qty;     
        
          //kurangi stok dari produk
          $produk->qty = $produkQty + $qty;
          $produk->save();

        return redirect()->back()->with('pesan', 'Item ' . $produkDeleted[$id]['produk_nama'] . ' telah dihapus dari keranjang')->cookie($cookie);
    }

    public function cekout() 
    {
        $kabupatenId = Auth::user()->kabupaten_id;
        $dataKota = RajaOngkir::kota()->find($kabupatenId);   
        $keranjang = json_decode(request()->cookie('keranjang'), true); 
        $arrKurir = [
            'jne' => 'JNE',
            'tiki' => 'TIKI',
            'pos' => 'POS'
        ];
        
        return view('cekout', compact(['dataKota', 'arrKurir', 'keranjang']));
    }

    public function hitungBiayaKirim(Request $request) {
        $userKabupatenId = Auth()->user()->kabupaten_id;
        $kurir = $request->kurir;
        $keranjang = json_decode(request()->cookie('keranjang'), true); 
        $efastcomKabupatenId = config('info.EFASTCOM_KABUPATEN_ID');

        $total_harga = 0;
        $total_berat = 0;

        //hitung berat
        foreach($keranjang as $k) {
            $total_berat += $k['produk_berat'] * $k['qty'];
            $total_harga += $k['produk_harga'] * $k['qty'];
        }

        //hitung ongkir
        $ongkir = RajaOngkir::ongkosKirim([
            'origin'        => $efastcomKabupatenId,     // ID kota/kabupaten asal
            'destination'   => $userKabupatenId,      // ID kota/kabupaten tujuan
            'weight'        => $total_berat,    // berat barang dalam gram
            'courier'       => $kurir    // kode kurir pengiriman: ['jne', 'tiki', 'pos'] untuk starter
        ])->get();

        $jsonOngkir = json_encode($ongkir[0]['costs']);        
        return $jsonOngkir;
    }

    public function prosesPesanan(Request $request) {
        $all = $request->all();

        $validator = Validator::make(request()->all(), [
            'kurir' => 'required',
            'paket' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        //raja ongkir cari kabupaten, provinsi dan kodepos
        $kabupatenId = Auth::user()->kabupaten_id;

        $dataKota = RajaOngkir::kota()->find($kabupatenId);  
        $kabupaten = $dataKota['city_name'];
        $provinsi = $dataKota['province'];
        $kode_pos = $dataKota['postal_code'];

        //tambahkan request ke order
        $request->request->add([
            'kabupaten' => $kabupaten,
            'provinsi'  => $provinsi,
            'kode_pos'  => $kode_pos
        ]);

        //simpan order
        $order = Order::create($request->all());
        
        //simpan detail order
        $keranjang = json_decode(request()->cookie('keranjang'), true); 

        foreach($keranjang as $k) {
            $order->produk()->attach($k['produk_id'], [
                'nama'      => $k['produk_nama'],
                'merek'     => $k['produk_merek'],
                'qty'       => $k['qty'],
                'harga'     => $k['produk_harga'],
                'satuan'    => $k['produk_satuan'],
                'diskon'    => $k['produk_diskon'],
                'berat'     => $k['produk_berat'],
                'gambar'    => $k['produk_gambar'],
                "created_at" => Carbon::now(), 
                "updated_at" => Carbon::now(), 
            ]);
        }

        //kirim tagihan by mail
        $user = Auth::user();

        Mail::to(Auth::user()->email)->send(new CheckoutEmail($user, $order, $keranjang));
       
        //hapus cookie
        Cookie::queue(Cookie::forget('keranjang'));

        return redirect('/')->with('pesan', 'Email tagihan telah dikirim keinbox anda, terima kasih telah berbelanja di toko kami');
    }
}
