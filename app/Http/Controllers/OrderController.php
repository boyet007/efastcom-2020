<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Datatables;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_title = 'Data Order'; 
        $form_title = 'Tambah Data Order';
        return view('admin.transaksi.order.index', compact(['sub_title', 'form_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataTable() 
    {
        $model = Order::with('user')->select('order.*');
          
        return Datatables::of($model)  
        ->addColumn('user', function($model) {
            return $model->user->email;
        })
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master-3', [
                     'model' => $model,
                     'url_show' => route('order.show', $model->id),
                     'url_edit' => route('order.edit', $model->id),
                     'url_destroy' => route('order.destroy', $model->id)
                 ]);   
            })
        ->rawColumns(['aksi'])
        ->make(true);
    }
}
