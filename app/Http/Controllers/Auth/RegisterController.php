<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\VerificationEmail;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use Auth;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('verifikasiToken');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(Request $data)
    // {
    //     return Validator::make($data, [
    //         'email' => ['required', 'string', 'email', 'max:150', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //         'nama_lengkap' => ['required', 'string', 'max:50'],
    //         'alamat' => ['required'],
    //         'kota_id' => ['required'],
    //         'kode_pos' => ['required', 'numeric', 'max:5'],
    //         'no_telp' => ['required', 'numeric', 'max:15'],
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:150|unique:users',
            'password' => 'required|string|between:5,10|confirmed',
            'nama_lengkap' => 'required|string|max:50',
            'alamat' => 'required',
            'kabupaten_id' => 'required',
            'no_telpon' => 'required|numeric|digits_between:5,15',
        ]);

        $password = Hash::make($request->password);
        $request->merge(['password' => $password]);

        $request->request->add(['verifikasi_token' => Str::random(64)]);
        $model = User::create($request->all());   

        Mail::to($model->email)->send(new VerificationEmail($model));

        Auth::login($model);

        session()->flash('pesan', 'Silahkan cek email anda untuk verifikasi akun');
        return redirect()->route('index');        
    }

    public function verifikasiToken($token = null) {
        if($token === null) {
    		session()->flash('pesan', 'Silahkan verifikasi akun anda terlebih dahulu');
    		return redirect()->route('login.user');
    	}

       $user = User::where('verifikasi_token',$token)->first();

       if($user === null ){
       	session()->flash('pesan', 'Token anda error, tekan F5 atau refresh untuk verifikasi akun anda kembali');
        return redirect()->back();
       }

       $user->status_verifikasi = true;
       $user->save();



       iF(Auth::check()) {
           session()->flash('pesan', 'Selamat akun anda telah diverifikasi');
           return redirect()->route('index');
       } else {
           return redirect()->route('login.user')
            ->with('pesan_verifikasi', 'Selamat akun anda telah diverifikasi, silahkan login');
       }


    }

}
