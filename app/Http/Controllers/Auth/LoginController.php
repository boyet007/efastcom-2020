<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Mail\VerificationEmail;
use Mail;
use Illuminate\Support\Str; 
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'verifikasiUlang']);
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }

        return redirect()->back()
            ->withInput($request->except('password'))
            ->with('error', 'Email atau Password anda salah');
    }

    public function verifikasiUlang($id) {
        
        $model = User::find($id);
        $model->verifikasi_token = Str::Random(64);
        $model->save();

        Mail::to($model->email)->send(new VerificationEmail($model));
        return redirect()->back()->with('pesan', 'Silahkan cek email anda untuk verifikasi');
    }

    public function logout() {
        Auth::logout();
        Cookie::queue(Cookie::forget('keranjang'));
        return redirect('/');
    }
}
