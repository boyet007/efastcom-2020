<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\KategoriProduk;
use App\Produk;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index() {
        $title = 'Selamat Datang di eFastcom';
        $indexKategori = KategoriProduk::get()->random(3);

        $baru = config('info.LIMIT_PRODUK_BARU');
        $produk = Produk::all();
        $produkBaru = collect();

        foreach($produk as $p) {
            $sekarang = Carbon::now();
            $tanggal_beli = Carbon::parse($p->tanggal_beli);
            $diff = $tanggal_beli->diffInDays($sekarang);
            if($diff < $baru) {
                $produkBaru->push($p);
            }
        }
       
        return view('index', compact('title', 'indexKategori', 'produkBaru'));       
    }

    public function dapatkanProduk() {
        $pagination = config('info.PAGINASI_HALAMAN');
        $produk = Produk::inRandomOrder('id')->paginate($pagination);
        return view('web.produk', compact('produk'));
    }

    public function spesifikProduk($id) {

        $produk = Produk::find($id);
        $kategoriId = $produk->kategori_produk->id;
        $semuaProduk = KategoriProduk::find($kategoriId)->produk;
        
        if($semuaProduk->count() >= 5) {
            $produkLain = $semuaProduk->except($id)->random(4);
        } else {
            $produkLain = $semuaProduk->except($id);
        }

        return view('web.spesifik-produk', compact('produk', 'produkLain'));
    }

    public function dapatkanKategoriProduk(Request $request) {
        $method = $request->method();
        $kategori_id = $request->kategori_id;
        $cari = $request->cari;
        $pagination = config('info.PAGINASI_HALAMAN');

        if(isset($kategori_id) && isset($cari)) {
            $produk = Produk::where('kategori_id', $kategori_id)->where('nama', 'LIKE', '%' . $cari . '%')->paginate($pagination);
        } else if (isset($kategori_id)) {
            $produk = Produk::where('kategori_id', $kategori_id)->paginate($pagination);
        } else if (isset($cari)) {
            $x = $cari;
            $produk = Produk::where('nama', 'LIKE', '%' . $cari . '%')->paginate($pagination);
        } else {
            $produk = Produk::inRandomOrder('id')->paginate($pagination);
        }

        $baru = config('info.LIMIT_PRODUK_BARU');
        
        return view('web.produk', compact('produk', 'baru'));  
    }

    public function dapatkanProdukPromo() {
        $pagination = config('info.PAGINASI_HALAMAN');
        $produk = Produk::where('diskon', '>', 0)->inRandomOrder('id')->paginate($pagination);
        $baru = config('info.LIMIT_PRODUK_BARU');
        return view('web.produk', compact('produk', 'baru'));
    }

    public function webKategoriProduk($id) {
        $pagination = config('info.PAGINASI_HALAMAN');
        $produk = Produk::where('kategori_id', $id)->paginate($pagination);
        $baru = config('info.LIMIT_PRODUK_BARU');
       
        return view('web.produk', compact('produk', 'baru'));  
    }
}
