<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Kota;
use App\User;
use DataTables;
use Kavist\RajaOngkir\Facades\RajaOngkir;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index() {
        $sub_title = 'Data User'; 
        return view('admin.master.user.index', compact('sub_title'));
    }

    public function destroy($id) {
        $model = User::findOrFail($id);
        $model->delete();
    }

    public function show($id)
    {
        $model = User::findOrFail($id);
        return view('admin.master.user.tampil', compact('model'));
    }
    

    public function loginUser() {
        if (Auth::user()) {
            $email = Auth::user()->email;
            return redirect('/')->with(['email']);
    }
        return view('user.login');
    }

    public function daftarUserBaru() {
        if (Auth::user()) {
            $email = Auth::user()->email;
            return redirect('/')->with(['email']);
        }

        $daftarKabupaten = RajaOngkir::kota()->all();
        $daftarProvinsi = RajaOngkir::provinsi()->all();

        return view('user.daftar', compact(['daftarProvinsi', 'daftarKabupaten']));
    }

    public function updateUser(Request $request, $id) {
        $this->validate($request, [
            'nama_lengkap' => 'required|string|max:50',
            'alamat' => 'required',
            'kabupaten_id' => 'required',
            'no_telpon' => 'required|numeric|digits_between:5,15',
        ]);

        $user = User::find($id);
        $user->update($request->all());   
        return redirect('/')->with('pesan', 'Data anda telah diupdate');
    } 

    public function rubahPassword(Request $request, $id) {
        $this->validate($request, [
            'password' => 'required|string|between:5,10|confirmed',
        ]);
        
        $password = Hash::make($request->password);
        $request->merge(['password' => $password]);

        $user = User::find($id);
        $user->update($request->all());   
        return redirect('/')->with('pesan', 'Password anda telah di rubah');
    }

    public function cekOut($id) {
        return view('user.cekout');
    }

    public function cariProvinsiId(Request $request) {
        $id = $request->kotaId;
        return RajaOngkir::kota()->find($id);
    }

    public function cariListKota(Request $request) {
        $id = $request->provinsiId;
        return RajaOngkir::kota()->dariProvinsi($id)->get();
    }

    public function profile($id) {
        if(Auth::user()->id === (int)$id) {
            $user = User::find($id);
            $daftarKabupaten = RajaOngkir::kota()->all();
            $daftarProvinsi = RajaOngkir::provinsi()->all();

            $userKabupaten = RajaOngkir::kota()->find($user->kabupaten_id);
            $user->setAttribute('kabupaten_id', $userKabupaten['city_id']);
            $user->setAttribute('provinsi_id', $userKabupaten['province_id']);
            $user->setAttribute('kode_pos', $userKabupaten['postal_code']);

            return view('user.profile', compact('user', 'daftarKabupaten', 'daftarProvinsi'));
        } else {
            return redirect()->route('index')->with('error', 'Maaf anda hanya bisa mengakses profile pribadi anda');
        }
    }
 
    public function dataTable() 
    {
        $model = User::query();
          
        return Datatables::of($model)  
        ->addColumn('aksi', function($model) {
                 return view('layouts.aksi._aksi-master-2', [
                     'model' => $model,
                     'url_show' => route('user.show', $model->id),
                     'url_destroy' => route('user.destroy', $model->id)
                 ]);   
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}

