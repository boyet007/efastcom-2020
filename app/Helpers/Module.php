<?php 

use Illuminate\Validation\ValidationException;

function cekNamaFile($parModel, $parFile) {
    if($parFile) {
        $namaGambar = $parFile->getClientOriginalName();
        $jumlahFile = $parModel->where('gambar', '=', $namaGambar)->count();
        if($jumlahFile > 0) {
            //return redirect()->back()->with('error', 'Nama file sudah ada, silahkan ganti nama file anda');
            throw ValidationException::withMessages(['file' => 'Nama file sudah ada, silahkan ganti nama file anda']);
        }
    }
}
