<?php

    function hitungUsiaProduk($tanggal) {
        $tanggal_lama = new DateTime($tanggal);
        $sekarang = new DateTime(now());
        return $sekarang->diff($tanggal_lama)->days + 1;
    }