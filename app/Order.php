<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = ['nama', 'kurir', 'paket', 'estimasi', 'ongkir', 'catatan', 'status_pembayaran', 'proses',
      'user_id', 'kabupaten', 'provinsi', 'kode_pos'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function produk() {
      return $this->belongsToMany(Produk::class)->withPivot([
        'qty',
        'harga',
        'diskon'
      ]);
    }
}
