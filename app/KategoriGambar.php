<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriGambar extends Model
{
    protected $table = 'kategori_gambar';
    protected $fillable = ['nama'];
    public $timestamps = false;

    public function gambar() {
        return $this->hasMany(Gambar::class, 'kategori_id', 'id');
        
    }
}
