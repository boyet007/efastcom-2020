<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Kavist\RajaOngkir\Facades\RajaOngkir;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'nama_lengkap', 'alamat', 'kabupaten_id',
        'no_telpon', 'role', 'verifikasi_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'verifikasi_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status_verifikasi' => 'boolean',
    ];

    public function setTokenVerifikasi() {
        $token = Str::random(60);
        $this->verifikasi_token = $token;
        $this->save();
    }

    public function order() {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    public function kabupaten() {
        $id = $this->kabupaten_id;
        $dataRajaOngkir = RajaOngkir::kota()->find($id);
        return $dataRajaOngkir['city_name'];
    }

    public function provinsi() {
        $id = $this->kabupaten_id;
        $dataRajaOngkir = RajaOngkir::kota()->find($id);
        return $dataRajaOngkir['province'];
    }

    public function kode_pos() {
        $id=$this->kabupaten_id;
        $dataRajaOngkir = RajaOngkir::kota()->find($id);
        return $dataRajaOngkir['postal_code'];
    }

    protected $cast = [
        'status_verifikasi' => 'boolean'
    ];

    public function verifikasi() {
        if ($this->status_verifikasi) {
            return 'sudah terverifikasi';
        } else {
            return 'belum terverifikasi';
        }
    }
}
