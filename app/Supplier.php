<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $fillable = ['nama', 'alamat', 'kota_id', 'kode_pos', 'no_telpon', 
        'email', 'website', 'kode_customer', 'pic', 'ket'];
    public $timestamps = false;

    public function kota() {
        return $this->belongsTo(Kota::class);
    }

    public function produk() {
        return $this->hasMany(Produk::class, 'supplier_id', 'id');
        //return $this->belongsToMany(Produk::class)->withPivot(['tanggal_beli', 'qty', 'harga_beli']);
    } 
}
