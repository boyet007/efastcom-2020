<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merek extends Model
{
    protected $table = 'merek';
    protected $fillable = ['nama', 'gambar'];
    public $timestamps = false;

    public function produk() {
        return $this->hasMany(Produk::class, 'merek_id', 'id');
    }

    public function supplier() {
        return $this->hasMany(Supplier::class);
    }
}
