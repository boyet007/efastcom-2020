/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/web-custom.js":
/*!************************************!*\
  !*** ./resources/js/web-custom.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function formatAngka(parAngka) {
  var number_string = parAngka.replace(/[^.\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      format = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

  if (ribuan) {
    separator = sisa ? ',' : '';
    format += separator + ribuan.join(',');
  }

  format = split[1] != undefined ? format + ',' + split[1] : format;
  return format;
}

function bikinHiddenInput(parElement, parValue) {
  //hapus hidden
  $('input[name=' + parElement + ']').remove();
  $('<input>').attr({
    type: 'hidden',
    name: parElement,
    value: parValue
  }).appendTo('form');
}

$('#provinsi_id').change(function (e) {
  e.preventDefault();

  if ($(this).val() !== '') {
    var provinsiId = $(this).val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/cari-list-kota",
      data: {
        provinsiId: provinsiId
      },
      type: "POST",
      success: function success(data) {
        var listKota = data;
        $('#kabupaten_id').empty();
        $('<option />', {
          val: '',
          text: '--- Pilih Kabupaten / Kota ---'
        }).appendTo('#kabupaten_id');
        $(listKota).each(function () {
          $("<option />", {
            val: this.city_id,
            text: this.city_name
          }).appendTo('#kabupaten_id');
        });
        $('#kode_pos').val('');
      },
      error: function error(_error) {
        console.log(_error);
      }
    });
  }
});
$('#kabupaten_id').change(function (e) {
  e.preventDefault();

  if ($(this).val() !== '') {
    var kotaId = $(this).val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/cari-provinsi-id",
      data: {
        kotaId: kotaId
      },
      type: "POST",
      success: function success(data) {
        var kota = data;
        $('#provinsi_id').val(kota['province_id']);
        $('#kode_pos').val(kota['postal_code']);
      },
      error: function error(_error2) {
        console.log(_error2);
      }
    });
  }
});
$('#tutup, #tutup_2').click(function (e) {
  e.preventDefault();
  $('.livechatbanner').css({
    'display': 'none'
  });
  $('.livechat').css({
    'display': 'block'
  });
});
$('.livechat').click(function (e) {
  e.preventDefault();
  $(this).css({
    'display': 'none'
  });
  $('#chatbox').css({
    'display': 'block'
  });
}); //live chat 

$(document).on('click', '.panel-heading span#minim_chat_window', function (e) {
  var $this = $(this);

  if (!$this.hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideUp();
    $this.removeClass('fa-window-minimize').addClass('fa-plus');
    $this.addClass('panel-collapsed');
  } else {
    $this.parents('.panel').find('.panel-body').slideDown();
    $this.removeClass('fa-plus').addClass('fa-window-minimize');
    $this.removeClass('panel-collapsed');
  }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
  var $this = $(this);

  if ($('#minim_chat_window').hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideDown();
    $('#minim_chat_window').removeClass('panel-collapsed');
    $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
  }
});
$(document).on('click', '#new_chat', function (e) {
  var size = $(".chat-window:last-child").css("margin-left");
  size_total = parseInt(size) + 400;
  alert(size_total);
  var clone = $("#chat_window_1").clone().appendTo(".container");
  clone.css("margin-left", size_total);
});
$(document).on('click', '.fa-window-close', function (e) {
  //$(this).parent().parent().parent().parent().remove();
  $("#chatbox").hide();
  $('.livechat').css({
    'display': 'block'
  });
}); // send function start

function send() {
  var chat = $("#btn-input").val();
  var dt = new Date();
  var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

  if (chat == "") {
    alert('Enter Message');
  } else {
    var body = '<div class="row msg_container base_sent">' + '<div class="col-md-10 col-xs-10 ">' + '<div class="messages msg_sent">' + '<p>' + chat + '</p>' + ' <time datetime="2009-11-13T20:00">Administrator • Today ' + time + '</time>' + '</div>' + '</div>' + '<div class="col-md-2 col-xs-2 avatar">' + '<img class="chatimg" src="https://cheme.mit.edu/wp-content/uploads/2017/01/stephanopoulosgeorge-431x400.jpg" class=" img-responsive ">' + '</div>' + '</div>';
  }

  $(body).appendTo("#messagebody");
  $('#btn-input').val('');
  $("#messagebody").animate({
    scrollTop: $("#messagebody")[0].scrollHeight
  }, 'slow');
} // send function end


$("#btn-chat").click(function () {
  send();
});

/***/ }),

/***/ "./resources/sass/admin-custom.scss":
/*!******************************************!*\
  !*** ./resources/sass/admin-custom.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/web-custom.scss":
/*!****************************************!*\
  !*** ./resources/sass/web-custom.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!**************************************************************************************************************!*\
  !*** multi ./resources/js/web-custom.js ./resources/sass/admin-custom.scss ./resources/sass/web-custom.scss ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! E:\Repo\efastcom-2020\resources\js\web-custom.js */"./resources/js/web-custom.js");
__webpack_require__(/*! E:\Repo\efastcom-2020\resources\sass\admin-custom.scss */"./resources/sass/admin-custom.scss");
module.exports = __webpack_require__(/*! E:\Repo\efastcom-2020\resources\sass\web-custom.scss */"./resources/sass/web-custom.scss");


/***/ })

/******/ });