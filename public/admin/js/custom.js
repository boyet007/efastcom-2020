function formatAngka(parAngka) {
  var number_string = parAngka.replace(/[^.\d]/g, '').toString(),
      split   		= number_string.split(','),
      sisa     		= split[0].length % 3,
      format    		= split[0].substr(0, sisa),
      ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  if(ribuan){
      separator = sisa ? ',' : '';
      format += separator + ribuan.join(',');
  }

  format = split[1] != undefined ? format + ',' + split[1] : format;
  return format;
}

function hitungDiskon(parHarga, parDiskon) {
  return hargaDiskon = parHarga - ( parHarga * parDiskon / 100 );
}

function bikinHiddenInput(element, angka) {
    //hapus hidden
    $('input[name=' + element + ']').remove();

    $('<input>').attr({
        type: 'hidden', name: element, value: angka
    }).appendTo('form');
}

function bacaUrl(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#data_gambar').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}


$('body').on('click', '.btn-tampil-modal', function(e) {
  e.preventDefault();
  var me =$(this),
  url = me.attr('href'),
  judul = me.attr('judul'),
  tipe = me.attr('tipe'),
  ukuran = me.attr('ukuran'),
  kode = me.data('kode')

  $('.close').addClass('d-none');
  $('#modal-title').text(judul);
  $('#modal-btn-save').text('Simpan');
  
  if(tipe === 'lihat') {
      $('#modal').attr('tipe', tipe);
      $('#modal-btn-close').text('Tutup');
      $('#modal-btn-save').addClass('d-none');
      $('#modal').attr('kode', kode);
    } else if(tipe === 'tambah') {
      $('#modal').attr('tipe', tipe);
      $('#modal-btn-save').removeClass('d-none');
      $('#modal-btn-close').text('Batal');
    } else if(tipe === 'edit') {
      $('#modal').attr('tipe', tipe);
      $('#modal-btn-save').removeClass('d-none');
      $('#modal-btn-save').text('Update');
      $('#modal-btn-close').text('Batal');
      $('#modal').attr('kode', kode);
    }

  $.ajax({
      url,
      dataType: 'html',
      success: function(response) {
          $('#modal-body').html(response)
      },
      error: function(error) {
          console.log(error);
      }
  })

  if (ukuran === 'besar') {
      $('.modal-dialog').addClass('modal-lg');
  }

  $('#modal').modal({backdrop:'static', keyboard:false})
  $('#modal').modal('show')

});

$('body').on('click', '.btn-hapus', function(e) {
  e.preventDefault();
  var me = $(this),
  url = me.attr('href'),
  judul = me.attr('judul'),
  csrf_token = $('meta[name="csrf-token"]').attr('content')

  swal.fire({
      title: 'Yakin ingin hapus data ' + judul + ' ?',
      text: 'Anda tidak akan mengambil data yang telah terhapus!',
      icon: 'warning',
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: 'rgb(220, 53, 69)',
      cancelButtonText: 'Tidak',
      confirmButtonText: 'Ya',
  }).then((result) => {
      if (result.value) {
          $.ajax({
              url,
              type: "POST",
              data: {
                  '_method': 'DELETE',
                  '_token': csrf_token
              },
              success: function(response) {
                  $('#datatable').DataTable().ajax.reload()
                  swal.fire({
                      icon: 'success',
                      title: 'Sukses',
                      text: 'Data telah dihapus'
                  })
              },
              error: function(xhr) {
                  swal.fire({
                      icon: 'error',
                      tittle: 'Oops...',
                      text: 'Oops, Ada Kesalahan, ' + xhr.statusText 
                  })
              }
          })
      }
  })
});

$('#modal-btn-save').click(function(event) {
  event.preventDefault()

  var form = $('#modal-body form'),
      url = form.attr('action')
  
      form.find('.form-text').remove()
      form.find('.form-group, .input-group, .card').removeClass('has-error')       

  var formData = new FormData($('#modal-body form')[0])  
      
  $('input[name=_method').val() == undefined ? null : formData.append('_method', 'PATCH')

  var method = formData.get('_method')
 
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $.ajax({
      url,
      method:'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function (response) {
          form.trigger('reset');
          $('#modal').modal('hide')
          $('#datatable').DataTable().ajax.reload()

          if (method !== "PUT") {
              //save
              Swal.fire({
                  icon: 'success',
                  title: 'Sukses!',
                  text: 'Data telah disimpan!'
              })
          } else {
              // update
            Swal.fire({
                  icon: 'success',
                  title: 'Sukses!',
                  text: 'Data telah diupdate!'
              })
          }
      },
      error: function (xhr) {            
          var res = xhr.responseJSON
          if ($.isEmptyObject(res) === false) {
              $.each(res.errors, function(key, value) {
                  $('#' + key)
                      .closest('.form-group, .input-group, .card')
                      .addClass('has-error')
                      .append('<span class="form-text text-danger"><strong>' + value +  '</strong></span>')
              });
          }   

          if (xhr.status === 500) { 
              swal.fire({
                  icon: 'error',
                  title: 'Gagal!',
                  text: 'Oops, Ada Kesalahan, ' + xhr.statusText 
              });
          }             
      } 
  })
})
