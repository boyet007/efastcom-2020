<?php

use Illuminate\Database\Seeder;
use App\KategoriProduk;

class KategoriProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        KategoriProduk::truncate();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'PC Rakitan';
        $kategori_produk->gambar = 'rakitan.jpg';
        $kategori_produk->save();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'Laptop';
        $kategori_produk->gambar = 'laptop.jpg';
        $kategori_produk->save();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'Handphone';
        $kategori_produk->gambar = 'product07.png';
        $kategori_produk->save();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'SparePart PC';
        $kategori_produk->gambar = 'sparepartpc.jpg';
        $kategori_produk->save();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'Network';
        $kategori_produk->gambar = 'lan.jpg';
        $kategori_produk->save();

        $kategori_produk = new KategoriProduk;
        $kategori_produk->nama  = 'Acessories';
        $kategori_produk->gambar = 'accesories.jpg';
        $kategori_produk->save();
    }
}
