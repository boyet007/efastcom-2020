<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KotaSeeder::class);
        $this->call(KategoriProdukSeeder::class);
        $this->call(MerekSeeder::class);
        $this->call(SatuanSeeder::class);
        $this->call(SupplierSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProdukSeeder::class);
    }
}
