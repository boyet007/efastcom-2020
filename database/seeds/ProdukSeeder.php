<?php

use Illuminate\Database\Seeder;
use App\Produk;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::truncate();

        $produk = new Produk;
        $produk->nama             = 'DDR Vgen 8 GB DDR 4 PC 19200';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 5;
        $produk->supplier_id      = 1;
        $produk->harga            = 986000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 850000;
        $produk->berat            = 200;
        $produk->gambar           = 'vgen8gb.png';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Acer Nitro Core i5 9400m Gaming Edition';
        $produk->kategori_id      = 2;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 2;
        $produk->harga            = 8950000;
        $produk->qty              = 3;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-01');
        $produk->harga_beli       = 8300000;
        $produk->berat            = 4500;
        $produk->gambar           = 'acer-nitro-laptop.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Altec Lansing 301 800 PMPO Clear Sound';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 6;
        $produk->supplier_id      = 5;
        $produk->harga            = 925000;
        $produk->qty              = 10;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-01');
        $produk->harga_beli       = 825000;
        $produk->berat            = 3000;
        $produk->gambar           = 'altec-lansing-3011.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Processor AMD Ryzen 3600 4.0 Ghz 8 Core ';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 4;
        $produk->supplier_id      = 3;
        $produk->harga            = 2350000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 2200000;
        $produk->berat            = 1000;
        $produk->gambar           = 'amd ryzen 3600.png';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Gigabyte Aeorus B450 Turbo Red Special Pro Wifi';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 4;
        $produk->harga            = 2500000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 2250000;
        $produk->berat            = 1500;
        $produk->gambar           = 'aorus-b450-pro-wifi.png';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Asus ROG 703 Gaming 9800M Edition Limited';
        $produk->kategori_id      = 2;
        $produk->merek_id         = 4;
        $produk->supplier_id      = 1;
        $produk->harga            = 12500000;
        $produk->qty              = 2;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 10600000;
        $produk->berat            = 7000;
        $produk->gambar           = 'asus-rog-g703.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Cashing INfinity Cooling Air Flow galaxy plus';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 4;
        $produk->harga            = 245000;
        $produk->qty              = 10;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 200000;
        $produk->berat            = 2000;
        $produk->gambar           = 'casing-infinity.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Harddisk Seagate 7200 RPM 1 TB SATA Connection';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 3;
        $produk->harga            = 685000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-02-20');
        $produk->harga_beli       = 600000;
        $produk->berat            = 800;
        $produk->diskon           = 25;
        $produk->gambar           = 'harddisk-seagatae-7200rpm-1gb.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Hdd SSD Adata SU 650 128 GB USB 3.0';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 5;
        $produk->harga            = 300000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 250000;
        $produk->berat            = 350;
        $produk->diskon           = 25;
        $produk->gambar           = 'adata2.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Intel Core i5 9400F 2.9 Ghz Box';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 5;
        $produk->supplier_id      = 4;
        $produk->harga            = 2100000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-20');
        $produk->harga_beli       = 1800000;
        $produk->berat            = 1200;
        $produk->gambar           = 'intelcoreia5.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Kabel Lan Merek TP LInk 1 Meter';
        $produk->kategori_id      = 5;
        $produk->merek_id         = 1;
        $produk->supplier_id      = 2;
        $produk->harga            = 5000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-01');
        $produk->harga_beli       = 2350;
        $produk->berat            = 50;
        $produk->gambar           = 'alan.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Kingston Flashdisk 32 Gb USB 3.0 White Edition';
        $produk->kategori_id      = 6;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 2;
        $produk->harga            = 76000;
        $produk->qty              = 20;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-01');
        $produk->harga_beli       = 50000;
        $produk->berat            = 100;
        $produk->gambar           = 'kingston-flashddisk 16 gb dt50.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Kingston RAM Hyper furi 8 GB PC 32000 DDR 4';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 5;
        $produk->supplier_id      = 1;
        $produk->harga            = 1250000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 1000000;
        $produk->berat            = 900;
        $produk->gambar           = 'kingston-hyper-x.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Lexar SSD 128 GB Black Edition';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 5;
        $produk->supplier_id      = 2;
        $produk->harga            = 315000;
        $produk->qty              = 10;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 250000;
        $produk->berat            = 150;
        $produk->gambar           = 'lexar-hdd.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'LG Monitor 19M33ab 21 Switching Fresh"';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 1;
        $produk->supplier_id      = 4;
        $produk->harga            = 1350000;
        $produk->qty              = 3;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 1140000;
        $produk->berat            = 2500;
        $produk->gambar           = 'lg-19m38ab.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Mainboard Asrock b365 Wifi pro';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 5;
        $produk->harga            = 2670000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 2400000;
        $produk->berat            = 1700;
        $produk->gambar           = 'mainboard-ascrok-b365m-gaming.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'PC Rakitan Gaming Black OP';
        $produk->kategori_id      = 1;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 3;
        $produk->harga            = 12500000;
        $produk->qty              = 2;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-04-28');
        $produk->harga_beli       = 10500000;
        $produk->berat            = 10000;
        $produk->gambar           = 'pc-rakitan.png';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'PC Rakitan Gaming PES 2020 with Core i5 9400F';
        $produk->kategori_id      = 1;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 3;
        $produk->harga            = 7600000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 7000000;
        $produk->berat            = 8500;
        $produk->gambar           = 'pc-rakitan-3.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'PC Rakitan Gaming Balap Spesial';
        $produk->kategori_id      = 1;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 3;
        $produk->harga            = 8500000;
        $produk->qty              = 5;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 7800000;
        $produk->berat            = 9000;
        $produk->gambar           = 'pc-rakitan-corei59400f-8gb.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Router Wireless TLWR841N New Edition 24Ghz';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 1;
        $produk->supplier_id      = 2;
        $produk->harga            = 245000;
        $produk->qty              = 14;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 190000;
        $produk->berat            = 2300;
        $produk->gambar           = 'router wireless tlwr841n.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'LED Samsung Fresh 21" tipe S22F350';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 2;
        $produk->supplier_id      = 1;
        $produk->harga            = 1250000;
        $produk->qty              = 10;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 1000000;
        $produk->berat            = 5000;
        $produk->gambar           = 'samsung-s22f350fe.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'USB Scanddisk Blade 32 GB USB 3.0';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 4;
        $produk->supplier_id      = 3;
        $produk->harga            = 60000;
        $produk->qty              = 20;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 50000;
        $produk->berat            = 200;
        $produk->ket              = 'With its stylish, compact design and generous capacity, the Cruzer Blade USB Flash Drive makes it easy to back up, transfer, and share your files. Available in capacities 16 GB & 32 GB, this USB drive lets you carry your photos, movies, music, and personal data wherever you go.';
        $produk->gambar           = 'usb scanddiska.jpg';
        $produk->save();

        $produk = new Produk;
        $produk->nama             = 'Cashing Vnom Full Air FLow Pro';
        $produk->kategori_id      = 4;
        $produk->merek_id         = 4;
        $produk->supplier_id      = 4;
        $produk->harga            = 340000;
        $produk->qty              = 10;
        $produk->satuan_id        = 3;
        $produk->tanggal_beli     = date('2020-01-21');
        $produk->harga_beli       = 300000;
        $produk->berat            = 1500;
        $produk->diskon           = 10;
        $produk->gambar           = 'venomrx-squadron-fr-template-glass.jpg';
        $produk->save();
    }
}
