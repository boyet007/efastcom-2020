<?php

use Illuminate\Database\Seeder;
use  App\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user = new User;
        $user->email             = 'boyet007@gmail.com';
        $user->password          = bcrypt('Wynne321');
        $user->nama_lengkap      = 'Philip Loa';
        $user->alamat            = 'Jl Kresna No. 188';
        $user->kabupaten_id      = 55;
        $user->no_telpon         = '0812 1088 5815';
        $user->role              = 'admin';
        $user->status_verifikasi = 1;
        $user->save();

        $user = new User;
        $user->email            = 'wynne_zong@yahoo.com';
        $user->password         = bcrypt('Wynne321');
        $user->nama_lengkap     = 'Wynne Kurniawan';
        $user->alamat           = 'Jl keadilan no. 5';
        $user->kabupaten_id      = 6;
        $user->no_telpon        = '0811 1198 1928';
        $user->status_verifikasi = 1;
        $user->save();
    }
}
