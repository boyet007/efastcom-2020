<?php

use Illuminate\Database\Seeder;
use App\Merek;


class MerekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Merek::truncate();

        $merek = new Merek;
        $merek->nama             = 'LG';
        $merek->gambar           = 'lglogo.jpg';
        $merek->save();

        $merek = new Merek;
        $merek->nama             = 'Samsung';
        $merek->gambar           = 'samsung-logo.png';
        $merek->save();

        $merek = new Merek;
        $merek->nama             = 'Philips';
        $merek->gambar           = 'philips-logo.png';
        $merek->save();

        $merek = new Merek;
        $merek->nama             = 'Scandisk';
        $merek->gambar           = 'SanDisk_logo.png';
        $merek->save();

        $merek = new Merek;
        $merek->nama             = 'Vgen';
        $merek->gambar           = 'vgen.png';
        $merek->save();

        $merek = new Merek;
        $merek->nama             = 'Intel';
        $merek->gambar           = 'intel.png';
        $merek->save();


    }
}
