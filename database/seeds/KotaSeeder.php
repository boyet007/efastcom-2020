jph<?php

use Illuminate\Database\Seeder;
use App\Kota;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kota::truncate();

        $kota = new Kota;
        $kota->kode             = 'jkt';
        $kota->nama             = 'Jakarta';
        $kota->save();

        $kota = new Kota;
        $kota->kode             = 'bdg';
        $kota->nama             = 'Bandung';
        $kota->save();

        $kota = new Kota;
        $kota->kode             = 'dpk';
        $kota->nama             = 'Depok';
        $kota->save();

        $kota = new Kota;
        $kota->kode             = 'bks';
        $kota->nama             = 'Bekasi';
        $kota->save();

        $kota = new Kota;
        $kota->kode             = 'tgg';
        $kota->nama             = 'Tanggerang';
        $kota->save();
    }
}
