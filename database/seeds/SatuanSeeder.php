<?php

use Illuminate\Database\Seeder;
use App\Satuan;

class SatuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Satuan::truncate();

        $satuan = new Satuan;
        $satuan->kode             = 'Kg';
        $satuan->nama             = 'Kilogram';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'G';
        $satuan->nama             = 'Gram';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Pcs';
        $satuan->nama             = 'Piesces';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Btg';
        $satuan->nama             = 'Batang';
        $satuan->save();

        $satuan = new Satuan;
        $satuan->kode             = 'Ktk';
        $satuan->nama             = 'Kotak';
        $satuan->save();
    }

}
