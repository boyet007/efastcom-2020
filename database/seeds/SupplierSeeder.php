<?php

use Illuminate\Database\Seeder;
use App\Supplier;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::truncate();

        $supplier = new Supplier;
        $supplier->nama             = 'PT. Vgen Internasional';
        $supplier->alamat           = 'Jl. abimannyu no. 7 Cempedek Selatan';
        $supplier->kota_id          = 1;
        $supplier->no_telpon        = '021 8822 1921';
        $supplier->email            = 'vgen@yahoo.co.id';
        $supplier->kode_pos         = '18292';
        $supplier->website          = 'www.vgen-internasional.com';
        $supplier->kode_customer    = 'P-1225';
        $supplier->pic              = 'Bpk. Indra';
        $supplier->save();

        $supplier = new Supplier;
        $supplier->nama             = 'PT. Sakura Indonesia';
        $supplier->alamat           = 'Jl. abimannyu no. 8 Cempedek Selatan';
        $supplier->kota_id          = 1;
        $supplier->kode_pos         = '18293';
        $supplier->no_telpon        = '021 8822 1922';
        $supplier->email            = 'sakura@yahoo.co.id';
        $supplier->website          = 'www.sakura.com';
        $supplier->kode_customer    = 'P-1226';
        $supplier->pic              = 'Bpk. Jonni';
        $supplier->ket              = 'Distributor laptop ryzen';
        $supplier->save();

        $supplier = new Supplier;
        $supplier->nama             = 'PT. Central IT';
        $supplier->alamat           = 'Jl. abimannyu no. 9 Cempedek Selatan';
        $supplier->kota_id          = 2;
        $supplier->kode_pos         = '18294';
        $supplier->no_telpon        = '021 8822 1923';
        $supplier->email            = 'central-it@yahoo.co.id';
        $supplier->website          = 'www.central-it.com';
        $supplier->kode_customer    = 'P-1227';
        $supplier->pic              = 'Bpk. Gunawan';
        $supplier->save();

        $supplier = new Supplier;
        $supplier->nama             = 'PT. Maju Mundur';
        $supplier->alamat           = 'Jl. abimannyu no. 10 Cempedek Selatan';
        $supplier->kota_id          = 3;
        $supplier->kode_pos         = '18295';
        $supplier->no_telpon        = '021 8822 1927';
        $supplier->email            = 'maju-mundur@yahoo.co.id';
        $supplier->website          = 'www.maju-mundur.com';
        $supplier->kode_customer    = 'P-1228';
        $supplier->pic              = 'Ibu Hendrawati';
        $supplier->ket             = 'Supplier accessories laptop';
        $supplier->save();

        $supplier = new Supplier;
        $supplier->nama             = 'PT. Sentosa Abadi';
        $supplier->alamat           = 'Jl. abimannyu no. 11 Cempedek Selatan';
        $supplier->kota_id          = 4;
        $supplier->kode_pos         = '18296';
        $supplier->no_telpon        = '021 8822 1917';
        $supplier->email            = 'sentosa-abadi@yahoo.co.id';
        $supplier->website          = 'www.sentosa-abadi.com';
        $supplier->kode_customer    = 'P-1229';
        $supplier->pic              = 'Bpk. Basuki';
        $supplier->ket             = 'Supplier Power Supply';
        $supplier->save();

        $supplier = new Supplier;
        $supplier->nama             = 'CMS indonesia';
        $supplier->alamat           = 'Jl. abimannyu no. 14 Cempedek Selatan';
        $supplier->kota_id          = 1;
        $supplier->kode_pos         = '18297';
        $supplier->no_telpon        = '021 8822 2000';
        $supplier->email            = 'cms@yahoo.co.id';
        $supplier->website          = 'www.cms-internasional.com';
        $supplier->kode_customer    = 'P-1230';
        $supplier->pic              = 'Bpk. Budi';
        $supplier->save();
    }
}
