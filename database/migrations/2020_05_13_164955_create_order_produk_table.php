<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_produk', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->integer('produk_id');
            $table->string('nama');
            $table->string('merek');
            $table->integer('qty');
            $table->double('harga');
            $table->string('satuan');
            $table->integer('diskon')->nullable();
            $table->integer('berat');
            $table->string('gambar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_produk');
    }
}
