<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('kategori_id');
            $table->integer('merek_id');
            $table->integer('supplier_id');
            $table->double('harga');
            $table->integer('qty');
            $table->integer('satuan_id');
            $table->integer('diskon')->nullable();
            $table->date('tanggal_beli');
            $table->double('harga_beli');
            $table->integer('berat');
            $table->text('ket')->nullable();
            $table->string('gambar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
