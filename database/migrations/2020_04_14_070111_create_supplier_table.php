<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->unnique();
            $table->text('alamat');
            $table->integer('kota_id');
            $table->string('kode_pos');
            $table->string('no_telpon');
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('kode_customer');
            $table->string('pic')->nullable();
            $table->text('ket')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
