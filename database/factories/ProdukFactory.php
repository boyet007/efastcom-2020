<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Produk;
use Faker\Generator as Faker;
use App\KategoriProduk;
use App\Merek;
use App\Supplier;
use App\Satuan;

$factory->define(Produk::class, function (Faker $faker) {
    return [
        'nama' => $faker->numerify('Produk ###'),
        'kategori_id' => KategoriProduk::all()->random()->id,
        'merek_id' => Merek::all()->random()->id,
        'supplier_id' => Supplier::all()->random()->id,
        'harga' => $faker->numberBetween($min=50000, $max=1000000),
        'qty' => $faker->numberBetween($min=5, $max=100),
        'satuan_id' => Satuan::all()->random()->id,
        'diskon' => $faker->numberBetween($min=10, $max=50),
        'tanggal_beli' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
        'harga_beli' => $faker->numberBetween($min=50000, $max=1000000),
        'ket' => $faker->sentences($nb = 3, $asText = false),
        'gambar' => $faker->randomElement(['product01.png', 'product03.png', 'product04.png', 'product06.png',
        'product08.png']),
    ];
});
