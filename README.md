akuntansi-web-muliajaya
=========================

pixel images :
background front : 1900 x 1267
galeri proyek : 500 x 700
admin photo avatar : 60 x 60


icon list
==========
https://mdbootstrap.com/docs/jquery/content/icons-list/


frontend template
===================

bootstrap template
===================
https://startbootstrap.com/previews/sb-admin-2/

icon list fa 
=============
https://fontawesome.com/v4.7.0/icons/

xdebug
=======
https://www.codewall.co.uk/debug-php-in-vscode-with-xdebug/
https://stackoverflow.com/questions/51685952/debugging-laravel-application-on-vscode --> setting json

xdebug launch json
=====================
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "configurations": [
        {
            "name": "Launch Program",
            "type": "php",
            "request": "launch",
            "cwd": "${workspaceRoot}",
            "port" : 9000
        },
        {
            "name": "Launch localhost",
            "request": "launch",
            "url": "http://localhost/public",
            "webRoot": "${workspaceRoot}"
        }
    ]
}

faker list :
$factory->define(App\User::class, function (Faker $faker) {
    static $password;
    return [
        'kdusr' => 'usr' . str_pad($faker->unique()->numberBetween(1, 90), 2, '0', STR_PAD_LEFT),
        'kdkat' => KategoriUser::all()->random()->kdkat,
        'username' => $faker->username,
        'password' => $password ? : $password = app('hash')->make('secret'),
        'nama' => $faker->name,
        'email' => $faker->email,
        'alamat' => $faker->address,
        'kdkot' => Kota::all()->random()->kdkot,
        'kdpos' => Address::postcode(),
        'telp' => $faker->phoneNumber,
        'token' => str_random(10),
        'foto' => 'unknown.jpg',
        'kdbank' => Bank::all()->random()->kdbank,
        'no_rek' => $faker->numerify('###.####.####.###'),
        'nama_akun_bank' => $faker->name
    ];
});


Faker Factory
=====================

create di artiasn : php artisan make:factory BookFactory --model=Book

$faker = Faker\Factory::create();
manggilnya lewat tinker : 
factory(App\User::class, 500)->create();


example jquery laravel pos sale 
=====================================
https://quickadminpanel.com/blog/master-detail-form-in-laravel-jquery-create-order-with-products/

access laravel server from other pc 
=========================================
sudo php artisan serve --host 192.168.1.101 --port 80


show images at domp pdf path
============================================
<img src="{{ public_path('admin/img/logo_baru.jpg') }}"  alt="Logo" class="logo"/>

margin invoice matrix
===========================================
margin: none


engineer template
=======================================
about : 1000 x 1280 
slide : 1900 x 1267
galeri : 500 x 700
tentang : 400 x 500
logo : 132 x 68

sisipkan gambar dari file config
===================================
<style="background-image: url('web/images/{{ config('situs.BERANDA_CAROUSEL_3_GAMBAR') }}')
<img src="{{ asset('web/images/mesin/' . config('situs.BERANDA_MESIN_1_GAMBAR')) }}" alt="Image" class="img-fluid">
url href  -> <a href="{{ url('/produk/' . $p->id) }}">

format tanggal
=================
moment(data).format('DD-MM-YYYY'); -> javascript;

{ data: 'tanggal', name: 'tanggal', 
    render:function(data) {
         return moment(data).format('DD-MM-YYYY');
    }
},

kalo dari controller php :
$model_tanggal = $model->tanggal;
$tanggal = date('d-m-Y', strtotime($model_tanggal));

untuk mengambil tahun dari tahun ini dicontroller : $tahun = date('Y');

format uang index blade dari datatable:
=========================================
{ data: 'harga', render: $.fn.dataTable.render.number('.', ',', 2, 'Rp ')},

kalo javascript : 
formatRupiah('5000', 'Rp. ')


Eager Loading
================
https://stackoverflow.com/questions/20036269/query-relationship-eloquent



file .env
==========
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

vscode collapse code
=====================
Use Ctrl + K + 0 to fold all and Ctrl + K + J to unfold all. Right-click anywhere in document and select "format document" option. Then hover next to number lines and you will see the (-) sign for collapsing method.


Efastcom-2020
===============
tabel :
1. kategori_produk x
2. produk 
    - nama
    - kategori_id
    - supplier_id
    - harga
    - qty
    - satuan_id
    - deskripsi
3. gambar
    - nama_file
    - produk_id
4. supplier
    - nama
    - pic
    - jabatan_pic
    - alamat
    - kota_id
    - kode_pos
    - telephon
    - email
    - website
    - pelanggan_id
    - deskripsi
5. satuan
    - kode
    - nama
6. kota
    - kode
    - nama
7. order    
    - customer_id
    - tanggal_order
    - tanggal_kirim
    - pengirim_id
    - tanggal_bayar
8. detail_order
    - order_id
    - produk_id
    - harga
    - qty
    - satuan_id
    - diskon
    - warna
9. pengirim
    - nama
    - deskripsi


Image
=========
produk : 600 x 600, jpg / png
indexKategoriProduk : 600 x 400


Validasi
===========
'email' => ['required', 'string', 'email', 'max:150', 'unique:users'],
'password' => ['required', 'between:5,10', 'string', 'confirmed'],
'nama_lengkap' => ['required', 'string', 'max:50'],
'alamat' => ['required'],
'kota_id' => ['required'],
'kode_pos' => ['required', 'numeric', 'digits:5'],
'no_telp' => ['required', 'numeric', 'max:15'],

digit satuan => max(3);
produk
'nama' => ['required', 'string', 'min:30', 'max:50'],



Mail verfication
https://www.codechief.org/article/laravel-6-activate-account-after-email-verification-using-laravel-mail
https://www.5balloons.info/user-email-verification-and-account-activation-in-laravel-5-5/

Mailchimp -> desain mail
username : boyet007
password : xxx 
Mailtrap.io -> testing mail sending

mail inline css link : https://templates.mailchimp.com/resources/inline-css/
jangan lupa hapus tag <style></style> stelah di inline

Collection
============
$collection = collect([1 => 'satu', 2 => 'dua', 3 => 'tiga']);
$item = [4 => 'empat', 5 => 'lima'];
$collection->prepend('nol',  0); //ditaruh diawal

foreach($item as $a) {
    $collection->push($a); //ditaruh diakhir
}

untuk menu combobox:
$kota = App\Kota::pluck('nama', 'id');
$kota->prepend('----silahkan pilih----', 0);
pluck mengembalikan collection sesuai kolom 



session
===========
https://www.malasngoding.com/session-laravel/

order ecomerce :
